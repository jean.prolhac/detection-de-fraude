# FraudDetect

## Description
FraudDetect is an application that allows you to generate data in the form (income, expenses, estate), obtain a probability of fraud and assign potential fraudulent cases to teams of auditors that can be easily formed.

## Installation
On Unix and MacOS: clone the project and in a terminal, activate the associated Python virtual environment with the following command:
```bash
cd path-name
source bin/activate
```

On Windows: clone the project and install the necessary modules via:
```bash
py -m pip install -r requirements.txt
```

Then simply run:
```bash
python frauddetect.py
```

## Architecture
The application has the following achitecture:
![architecture](images/architecture.png)

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.
Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)
