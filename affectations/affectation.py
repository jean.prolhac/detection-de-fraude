import pandas as pd
import numpy as np
from math import pi, atan, log, ceil
from ortools.linear_solver import pywraplp
from ortools.algorithms import pywrapknapsack_solver
from itertools import product
from mip import Model, xsum, maximize, BINARY
import time
import sys
sys.path.append('.')


def difficulty_index(wage, estate, proba):
    return abs(wage*estate)/(20*(proba**2-proba+0.3))


def dangerosity_index(cat, serie, dico, data_list):
    return max([(2/pi)*atan((serie[data_name]-dico[cat]['wage'][0])/dico[cat]['wage'][1]) for data_name in data_list])


def get_stats(data, data_list):
    stats_dico = {}
    for cat_id in pd.unique(data['cat']):
        stats_cat_id = {}
        for data_name in data_list:
            stats_cat_id[data_name] = [data[[data_name]].loc[data['cat']==cat_id].mean().to_numpy()[0],
                                       data[[data_name]].loc[data['cat']==cat_id].std().to_numpy()[0]]
        stats_dico[cat_id] = stats_cat_id
    return stats_dico


def get_list_of_members(selected_teams):
    list_of_members = []
    for team_id in selected_teams:
        list_of_members.extend(selected_teams[team_id])
    return list_of_members


def prepare_data(to_affect, list_of_members, proba_column_name, progress=None):
    data_list = ['wage', 'estate', 'expenses']
    stats_dico = get_stats(to_affect, data_list)
    to_affect['dangerosity_index'] = to_affect.apply(lambda x: dangerosity_index(x['cat'], x, stats_dico, data_list), axis=1)
    if not progress is None:
        progress[0]['value'] = 5
        progress[1].configure(text="5%")
        progress[2].update()
    to_affect['dangerosity'] = to_affect.apply(lambda x: x['dangerosity_index']*x[proba_column_name], axis=1)
    if not progress is None:
        progress[0]['value'] = 10
        progress[1].configure(text="10%")
        progress[2].update()
    to_affect['difficulty_index'] = to_affect.apply(lambda x: difficulty_index(x['wage'], x['estate'], x[proba_column_name]), axis=1)
    if not progress is None:
        progress[0]['value'] = 15
        progress[1].configure(text="15%")
        progress[2].update()
    to_affect['difficulty'] = to_affect.apply(lambda x: (2/pi)*atan(x['difficulty_index']/(10**9)), axis=1)
    if not progress is None:
        progress[0]['value'] = 20
        progress[1].configure(text="20%")
        progress[2].update()
    to_affect['amount'] = to_affect.apply(lambda x: x['dangerosity']*(x['wage']+x['estate'])/2, axis=1)
    if not progress is None:
        progress[0]['value'] = 25
        progress[1].configure(text="25%")
        progress[2].update()
    for member_id, experience in list_of_members:
        to_affect['processing_time_{}'.format(member_id)] = to_affect.apply(lambda x: x['difficulty']/log(0.5*experience+2), axis=1)
    if not progress is None:
        progress[0]['value'] = 30
        progress[1].configure(text="30%")
        progress[2].update()
    return to_affect


def knapsack_affectation(database, selected_teams, max_charge, value_id=0, progress=None, threshold=0, solver_type=0):
    start_time = time.time()
    list_of_members = get_list_of_members(selected_teams)
    proba_column_name = list(database.columns)[-1]
    to_affect = prepare_data(database, list_of_members, proba_column_name, progress)
    to_affect = to_affect.loc[to_affect[proba_column_name]>=threshold]
    data = {}
    if value_id == 0:
        data['values'] = to_affect['dangerosity'].to_numpy()
    elif value_id == 1:
        data['values'] = to_affect['probability'].to_numpy()
    elif value_id == 2:
        data['values'] = to_affect['dangerosity_index'].to_numpy()
    else:
        data['values'] = to_affect['amount'].to_numpy()
    if solver_type <= 1:
        data['weights'] = [to_affect['processing_time_{}'.format(member[0])].to_numpy() for member in list_of_members]
        #data['weights'] = [list(np.array(np.ceil(to_affect['processing_time_{}'.format(member[0])].to_numpy()), dtype=int)) for member in list_of_members]
        data['num_items'] = len(data['values'])
        data['all_items'] = range(data['num_items'])
        data['bin_capacities'] = [max_charge]*len(list_of_members)
        data['num_bins'] = len(data['bin_capacities'])
        data['all_bins'] = range(data['num_bins'])
        if solver_type == 0:
            solver = pywraplp.Solver.CreateSolver('SCIP')
            if solver is None:
                print('SCIP solver unavailable.')
            # x[i, b] = 1 if item i is packed in bin b.
            x = {}
            for i in data['all_items']:
                for b in data['all_bins']:
                    x[i, b] = solver.BoolVar(f'x_{i}_{b}')
            # Each item is assigned to at most one bin.
            for i in data['all_items']:
                solver.Add(sum(x[i, b] for b in data['all_bins']) <= 1)
            # The amount packed in each bin cannot exceed its capacity.
            for b in data['all_bins']:
                solver.Add(
                    sum(x[i, b] * data['weights'][b][i]
                        for i in data['all_items']) <= data['bin_capacities'][b])
            # Maximize total value of packed items.
            objective = solver.Objective()
            for i in data['all_items']:
                for b in data['all_bins']:
                    objective.SetCoefficient(x[i, b], data['values'][i])
            objective.SetMaximization()
            status = solver.Solve()
            if not progress is None:
                progress[0]['value'] = 100
                progress[1].configure(text="100%")
                progress[2].update()
            if status == pywraplp.Solver.OPTIMAL:
                data_dico = {}
                for b in data['all_bins']:
                    member_id = str(list_of_members[b][0])
                    team_id = member_id[:-1]
                    if team_id not in data_dico:
                        data_dico[team_id] = {}
                    data_dico[team_id][member_id] = {"result": {}, "exp": list_of_members[b][1]}
                    bin_weight = 0
                    for i in data['all_items']:
                        if x[i, b].solution_value() > 0:
                            data_dico[team_id][member_id]["result"][str(i)] = {"dangerosity": to_affect.iloc[i]['dangerosity'],
                                                                            "difficulty": to_affect.iloc[i]['difficulty'],
                                                                            "amount": to_affect.iloc[i]['amount'],
                                                                            "probability": to_affect.iloc[i][proba_column_name]}
                            bin_weight += data['weights'][b][i]
                    data_dico[team_id][member_id]["charge"] = bin_weight
                    data_dico[team_id][member_id]["nbre"] = len(data_dico[team_id][member_id]["result"])
                end_time = time.time()
                result_dico = {"data": data_dico, "params": {"sort": value_id, "time": end_time-start_time, "charge": max_charge, "threshold": threshold, "count": len(data['values']), "method": solver_type}}
                return result_dico
            else:
                return {}
        else:
            #data['values_2'] = [int(max(100*elem, 0)) for elem in data['values']]
            #data['weights_2'] = [[ceil(1000*elem) for elem in elem_list] for elem_list in data['weights']]
            #max_charge_2 *= 1000
            m = Model("knapsack")
            I_knap = list(product(data['all_bins'], data['all_items']))
            convert_dico_1 = {i: elem for i, elem in enumerate(I_knap)}
            convert_dico_2 = {elem: i for i, elem in enumerate(I_knap)}
            x_item = [m.add_var(var_type=BINARY) for _ in I_knap]
            m.objective = maximize(xsum(data['values'][elem[1]] * x_item[i] for i, elem in enumerate(I_knap)))
            for b in data['all_bins']:
                m += xsum(data['weights'][b][i] * x_item[b*data['num_items']+i] for i in data['all_items']) <= max_charge
            for i in data['all_items']:
                m += xsum(x_item[convert_dico_2[(b, i)]] for b in data['all_bins']) <= 1
            #m.optimize(max_seconds=900)
            m.emphasis = 1
            m.optimize(max_seconds=900, max_seconds_same_incumbent=300)
            selected = sorted([convert_dico_1[i] for i in range(len(I_knap)) if x_item[i].x >= 0.99], key=lambda x: x[0])
            if not progress is None:
                progress[0]['value'] = 100
                progress[1].configure(text="100%")
                progress[2].update()
            try:
                data_dico = {}
                for b in data['all_bins']:
                    member_id = str(list_of_members[b][0])
                    team_id = member_id[:-1]
                    if team_id not in data_dico:
                        data_dico[team_id] = {}
                    data_dico[team_id][member_id] = {"result": {}, "exp": list_of_members[b][1]}
                    bin_weight = 0
                    for elem in selected:
                        if elem[0] == b:
                            i = elem[1]
                            data_dico[team_id][member_id]["result"][str(i)] = {"dangerosity": to_affect.iloc[i]['dangerosity'],
                                                                            "difficulty": to_affect.iloc[i]['difficulty'],
                                                                            "amount": to_affect.iloc[i]['amount'],
                                                                            "probability": to_affect.iloc[i][proba_column_name]}
                            bin_weight += data['weights'][b][i]
                    data_dico[team_id][member_id]["charge"] = bin_weight
                    data_dico[team_id][member_id]["nbre"] = len(data_dico[team_id][member_id]["result"])
                end_time = time.time()
                result_dico = {"data": data_dico, "params": {"sort": value_id, "time": end_time-start_time, "charge": max_charge, "threshold": threshold, "count": len(data['values']), "method": solver_type}}
                return result_dico
            except:
                return {}
    else:
        #print(max_charge)
        try:
            T = []
            for _ in range(len(to_affect)):
                T.append(-1)
            possible_values = list(data['values'])
            for k, member in enumerate(list_of_members):
                # Create the solver.
                solver = pywrapknapsack_solver.KnapsackSolver(
                    pywrapknapsack_solver.KnapsackSolver.
                    KNAPSACK_MULTIDIMENSION_BRANCH_AND_BOUND_SOLVER, 'KnapsackExample')
                values = []
                for j, value in enumerate(possible_values):
                    if T[j] == -1:
                        values.append(int(max(100*value, 0)))
                    else:
                        values.append(0)
                weights = [[ceil(1000*elem) for elem in list(to_affect['processing_time_{}'.format(member[0])].to_numpy())]]
                capacities = [1000*max_charge]
                solver.Init(values, weights, capacities)
                solver.set_time_limit(180)
                solver.Solve()
                packed_items = []
                for i in range(len(values)):
                    if solver.BestSolutionContains(i):
                        packed_items.append(i)
                        T[i] = k
            data_dico = {}
            count = 0
            for j in range(len(T)):
                k = T[j]
                if k > -1:
                    member_id = str(list_of_members[k][0])
                    team_id = member_id[:-1]
                    if team_id not in data_dico:
                        data_dico[team_id] = {}
                    if member_id not in data_dico[team_id]:
                        data_dico[team_id][member_id] = {"result": {}, "exp": list_of_members[k][1], "charge": 0, "nbre": 0}
                    data_dico[team_id][member_id]["result"][str(j)] = {"dangerosity": to_affect.iloc[j]['dangerosity'],
                                                                    "difficulty": to_affect.iloc[j]['difficulty'],
                                                                    "amount": to_affect.iloc[j]['amount'],
                                                                    "probability": to_affect.iloc[j][proba_column_name]}
                    data_dico[team_id][member_id]["nbre"] += 1
                    data_dico[team_id][member_id]["charge"] += to_affect['processing_time_{}'.format(list_of_members[k][0])].iloc[j]
                    count += 1
            end_time = time.time()
            if not progress is None:
                    progress[0]['value'] = 100
                    progress[1].configure(text="100%")
                    progress[2].update()
            result_dico = {"data": data_dico, "params": {"sort": value_id, "time": end_time-start_time, "charge": max_charge, "threshold": threshold, "count": len(data['values']), "method": solver_type}}
            return result_dico
        except:
            return {}



#to_affect = pd.read_csv("./data/generated_data.tsv", sep='\t')
#selected_teams = {1: [(11, 3), (12, 1)], 2: [(21, 7), (22, 2)]}
#print(knapsack_affectation(to_affect, selected_teams, 5, 0, None, 0.5, 1))
