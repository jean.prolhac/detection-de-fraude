import pandas as pd
import numpy as np
import tkinter as tk
from tkinter import ttk
import matplotlib.pyplot as plt
import sys
from matplotlib.figure import Figure
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib import cm
import json

sys.path.append('.')
from affectations.affectation import knapsack_affectation
import gui.gui_config as config


def get_data():
    database = pd.read_csv("./data/generated_data.tsv", sep="\t")
    if not len(list(database.columns)[-1]) > 6:
        return []
    else:
        return database[['id', 'wage', 'estate', 'cat', 'expenses', list(database.columns)[-1]]]


def convert_teams(to_convert):
    convert_keys_list = list(to_convert.keys())
    if len(convert_keys_list) > 0:
        n = len(to_convert[convert_keys_list[0]])
        equipes = {}
        conversion_dico = {}
        for i, team_name in enumerate(to_convert):
            team = to_convert[team_name]
            converted_team = []
            if not len(team) == n:
                return (None, None)
            else:
                for j, verif in enumerate(team):
                    verif_id = int("{}{}".format(i+1, j+1))
                    converted_team.append((verif_id, verif[1]))
                    conversion_dico[verif_id] = verif[0]
                equipes[i+1] = converted_team
                conversion_dico[i+1] = team_name
        return (equipes, conversion_dico)
    else:
        return (None, None)


NO_ERROR = 0
NBR_ERROR = 1
MBM_ERROR = 2
LST_ERROR = 3
BND_ERROR = 4
BLC_ERROR = 5
CANCEL_ACTIVE = False
DISABLED_ACTIVE = False
current_state = {'global': NO_ERROR, 1: NBR_ERROR, 2: NBR_ERROR, 'team': NO_ERROR}
error_dico = {}
to_disable = {}
to_reset = {}
button_dico = {}
frames = {}
team_dico = {}
messages_bundle = {}
sort_etiqs = []
algo_etiqs = []
lang = None
cursor = None
conn = None
team_cat = {}


def update_state(lbl):
    global current_state
    current_state['global'] = max(list(current_state.values())[1:])
    lbl.configure(foreground=error_dico[current_state['global']][0], text=error_dico[current_state['global']][1])


def check_int(var, entry, lbl, id, *args):
    global current_state
    global button_dico
    button_dico['reset'].configure(state='normal')
    entry.configure(highlightbackground="#f1f1f1")
    current_state[id] = NO_ERROR
    update_state(lbl)
    trace_function()


def check_all(var, dico):
    if var.get() == 0:
        for key in dico:
            dico[key][1].deselect()
    else:
        for key in dico:
            dico[key][1].select()


def check_indiv(dico, button, lbl):
    global current_state
    vrai_s = True
    vrai_0 = True
    vrai_1 = True
    for key in dico:
        if vrai_s and dico[key][0].get() == 1:
            vrai_s = False
        if dico[key][0].get() == 1:
            vrai_0 = False
        if dico[key][0].get() == 0:
            vrai_1 = False
    if vrai_s:
        current_state['team'] = MBM_ERROR
    else:
        current_state['team'] = NO_ERROR
    update_state(lbl)
    if vrai_1:
        button.select()
    else:
        button.deselect()
    trace_function()


def disable():
    global to_disable
    global DISABLED_ACTIVE
    DISABLED_ACTIVE = True
    for key in to_disable:
        if to_disable[key][1] == 0:
            to_disable[key][0].configure(state='disabled')
        else:
            to_disable[key][0].config(state=tk.DISABLED)


def reset():
    global to_reset
    global button_dico
    for key in to_reset:
        label = to_reset[key]
        if label[1] == 'i':
            label[0].set(100)
        elif label[1] == 's':
            label[0].set('0')
        elif label[1] == 'l':
            label[0].configure(text="")
            to_reset[key] = (label[0], label[1], 0)
    button_dico['reset'].configure(state='disabled')


def max_func(bound):
    global to_reset
    to_reset['varCase'][0].set(str(bound))


def trace_function():
    global current_state
    global to_reset
    global button_dico
    global CANCEL_ACTIVE
    button_dico['reset'].configure(state='normal')
    label = to_reset['error']
    if current_state['global'] == NO_ERROR:
        if not CANCEL_ACTIVE:
            button_dico['apply'].configure(state='normal')
            label[0].configure(text="")
            to_reset['error'] = (label[0], label[1], 0)
        else:
            label[0].configure(text=messages_bundle["apply_clear"])
            to_reset['error'] = (label[0], label[1], 1)
    else:
        label[0].configure(text="")
        to_reset['error'] = (label[0], label[1], 0)
        button_dico['apply'].configure(state='disabled')


def get_color(per):
    if per > 1:
        return 'darkred'
    elif per >= 0.75:
        return 'limegreen'
    elif per >= 0.5:
        return 'gold'
    elif per >= 0.25:
        return 'orange'
    else:
        return 'red'


def convert_numbers(number,long_nbr, lang):
    if lang=="fr":
        if long_nbr:
            return "{:,.2f}".format(number).replace(',', ' ').replace('.', ',')
        else:
            return "{}".format(number).replace('.', ',')
    else:
        if long_nbr:
            return "{:,.2f}".format(number)
        else:
            return str(number)


def save_affect(result):
    save_file = open("./data/data.json", "w")
    json.dump(result, save_file)
    save_file.close()


def plot_affect_from_save_dico(result_frame, big_dict, conversion_dico, result=None, database=None, max_charge=None, selected_teams=None):
    count = 0
    res_dict = big_dict['data']
    L = []
    canvas = tk.Canvas(result_frame, background="#f1f1f1", width=798, height=400, highlightthickness=0, bd=0)
    canvas.pack(side='left', fill='both', expand=True)
    yscroll = ttk.Scrollbar(result_frame, orient='vertical', command=canvas.yview)
    yscroll.pack(side='right', fill='y', expand=0)
    xscroll = ttk.Scrollbar(result_frame, orient='horizontal', command=canvas.xview)
    canvas.configure(yscrollcommand=yscroll.set, xscrollcommand=xscroll.set)
    canvas.bind('<Configure>', lambda e: canvas.configure(scrollregion=canvas.bbox('all')))
    frame = tk.Frame(canvas, background="#f1f1f1", width=798)
    test = canvas.create_window((0, 0), window=frame, anchor='nw')
    for i, team_name in enumerate(res_dict):
        try:
            converted_team_name = conversion_dico[int(team_name)]
        except KeyError:
            converted_team_name = "{}_{}".format(messages_bundle['missing'], team_name)
        L.append(converted_team_name)
        treeview = ttk.Treeview(frame, columns=('Exp', messages_bundle["charge"], 'Nbr'))
        treeview.column('Exp', width=60, anchor='center')
        treeview.column(messages_bundle["charge"], width=60, anchor='center')
        treeview.column('Nbr', width=60, anchor='center')
        treeview.heading('#0', text=messages_bundle["inspectors"])
        treeview.heading('Exp', text='Exp')
        treeview.heading(messages_bundle["charge"], text=messages_bundle["charge"])
        treeview.heading('Nbr', text='Nbr')
        for verif in res_dict[team_name]:
            item_name_verif = 'item_v_{}'.format(verif)
            try:
                converted_verif_name = conversion_dico[int(verif)]
            except KeyError:
                converted_verif_name = "{}_{}".format(messages_bundle['missing'], verif)
            treeview.insert('', 'end', item_name_verif, text=converted_verif_name, values=(res_dict[team_name][verif]['exp'], round(res_dict[team_name][verif]['charge'],3), res_dict[team_name][verif]['nbre']))
            for case in res_dict[team_name][verif]['result']:
                count += 1
                item_name_case = 'item_c_{}'.format(case)
                treeview.insert(item_name_verif, 'end', item_name_case, text=case)
                treeview.insert(item_name_case, 'end', 'dang_{}'.format(case), text=messages_bundle["dangerosity"]+": "+convert_numbers(round(res_dict[team_name][verif]['result'][case]['dangerosity'],3), False, lang))
                treeview.insert(item_name_case, 'end', 'diff_{}'.format(case), text=messages_bundle["difficulty"]+": "+convert_numbers(round(res_dict[team_name][verif]['result'][case]['difficulty'],3), False, lang))
                treeview.insert(item_name_case, 'end', 'mnt_{}'.format(case), text=messages_bundle["amount"]+": "+convert_numbers(round(res_dict[team_name][verif]['result'][case]['amount'],1), True, lang))
                treeview.insert(item_name_case, 'end', 'prob_{}'.format(case), text=messages_bundle["probability"]+": "+convert_numbers(round(res_dict[team_name][verif]['result'][case]['probability'],3), False, lang))
        ttk.Label(frame, text=converted_team_name, anchor=tk.CENTER, font='Helvetica 14 bold').grid(row=2*(i//2), column=(2*i)%4, sticky='nsew')
        treeview.grid(row=2*(i//2)+1, column=(2*i)%4, sticky='nsew', pady=10)
        scrollbar = ttk.Scrollbar(frame, orient=tk.VERTICAL, command=treeview.yview)
        treeview.configure(yscroll=scrollbar.set)
        scrollbar.grid(row=2*(i//2)+1, column=(2*i)%4+1, sticky='ns')
        ttk.Label(frame, text=messages_bundle['select_teams']+": {}".format(', '.join(L))).grid(row=2*(len(res_dict)//2)+2, column=0, sticky='w', columnspan=4)
        ttk.Label(frame, text=messages_bundle['sort_met']+": {}".format(sort_etiqs[int(big_dict['params']['sort'])])).grid(row=2*(len(res_dict)//2)+3, column=0, sticky='w', columnspan=4)
        ttk.Label(frame, text=messages_bundle['algorithm']+": {}".format(algo_etiqs[int(big_dict['params']['method'])])).grid(row=2*(len(res_dict)//2)+4, column=0, sticky='w', columnspan=4)
        ttk.Label(frame, text=messages_bundle['time']+": {}".format(round(big_dict['params']['time'],1))).grid(row=2*(len(res_dict)//2)+2, column=1, sticky='w', columnspan=4)
        ttk.Label(frame, text=messages_bundle['threshold']+": {}".format(int(100*big_dict['params']['threshold']))).grid(row=2*(len(res_dict)//2)+3, column=1, sticky='w', columnspan=4)
        ttk.Label(frame, text=messages_bundle['max_load']+": {}".format(big_dict['params']['charge'])).grid(row=2*(len(res_dict)//2)+4, column=1, sticky='w', columnspan=4)
    if result is not None:
        button_dico['stats'] = ttk.Button(frame, text=messages_bundle["stat_button"], state='normal', command=lambda: statistics(count, big_dict['params']['count'], result, max_charge, selected_teams, conversion_dico))
        button_dico['stats'].grid(row=2*(len(res_dict)//2)+5, columnspan=4, pady=10)
        ttk.Label(frame, text="").grid(row=2*(len(res_dict)//2)+6, column=0, sticky='w', columnspan=4)
    else:
        ttk.Label(frame, text="").grid(row=2*(len(res_dict)//2)+5, column=0, sticky='w', columnspan=4)


def apply(current, win, database, teams, conversion_dico):
    global to_reset
    global button_dico
    global CANCEL_ACTIVE
    global frames
    global messages_bundle
    button_dico['apply'].configure(state='disabled')
    k = int(to_reset['varCase'][0].get())
    index_cas = to_reset['varSort'][0].get()
    index_algo = int(to_reset['varAlgo'][0].get())
    max_charge = int(to_reset['varCharge'][0].get())
    result_frame = tk.Frame(current, background="#f1f1f1")
    selected_teams = get_teams(teams)
    progress_frame = tk.Frame(current, background="#f1f1f1")
    progress_bar = ttk.Progressbar(progress_frame, orient='horizontal', mode='determinate', length=280)
    progress_bar.pack()
    progress_label = ttk.Label(progress_frame, text="", anchor=tk.CENTER)
    progress_label.pack()
    progress_frame.pack()
    win.update()
    result = knapsack_affectation(database, selected_teams, max_charge, index_cas, (progress_bar, progress_label, win), k/100, index_algo)
    progress_frame.destroy()
    win.update()
    if len(result) == 0:
        label = to_reset['error']
        label[0].configure(text=messages_bundle["no_result"])
        to_reset['error'] = (label[0], label[1], 2)
    else:
        save_affect(result)
        save_file = open("./data/data.json", "r")
        res_dict = json.load(save_file)
        save_file.close()
        plot_affect_from_save_dico(result_frame, res_dict, conversion_dico, result, database[['id', 'wage', 'estate', 'cat', list(database.columns)[-1]]].to_numpy(), max_charge, selected_teams)
    result_frame.pack(fill='both', expand=True)
    frames['result'] = result_frame
    button_dico['cancel'].configure(state='normal')
    CANCEL_ACTIVE = True


def cancel():
    global frames
    global to_reset
    global CANCEL_ACTIVE
    global button_dico
    frames['result'].destroy()
    CANCEL_ACTIVE = False
    button_dico['cancel'].configure(state='disabled')
    label = to_reset['error']
    if label[2] == 1:
        button_dico['apply'].configure(state='normal')
        label[0].configure(text="")
        to_reset['error'] = (label[0], label[1], 0)
    elif label[2] == 2:
        reset()
        label[0].configure(text="")
        to_reset['error'] = (label[0], label[1], 0)


def get_teams(teams):
    global to_reset
    team_list = list(teams.keys())
    selected_teams = {}
    for i in range(len(team_list)):
        team_name = team_list[i]
        if to_reset['name_check_{}'.format(i)][0].get() == 1:
            selected_teams[team_name] = teams[team_name]
    return selected_teams


def dico_to_df(dico, teams, max_value, conversion_dico):
    team = []
    verif = []
    max_list = []
    nb, mnt, dang, diff, prob, charge, exp = [], [], [], [], [], [], []
    for team_name in teams:
        for verif_name in teams[team_name]:
            verif_i = str(verif_name[0])
            team.append(conversion_dico[int(team_name)])
            verif.append(conversion_dico[int(verif_i)])
            nb.append(dico[verif_i][0])
            mnt.append(dico[verif_i][1])
            dang.append(dico[verif_i][2])
            diff.append(dico[verif_i][3])
            prob.append(dico[verif_i][4])
            charge.append(dico[verif_i][5])
            exp.append(dico[verif_i][6])
            if max_value is not None:
                max_list.append(max_value)
    temp_dico_1 = {messages_bundle["teams"]: team, messages_bundle["inspectors"]: verif, "Nbr": nb, messages_bundle["amount"]: mnt, messages_bundle["dangerosity"]: dang, messages_bundle["difficulty"]: diff, messages_bundle["probability"]: prob, messages_bundle["charge"]: charge, "Exp": exp}
    if max_value is not None:
        temp_dico_2 = {messages_bundle["teams"]: team, messages_bundle["inspectors"]: verif, "Max": max_list}
        return (pd.DataFrame(temp_dico_1, columns=list(temp_dico_1.keys())), pd.DataFrame(temp_dico_2, columns=list(temp_dico_2.keys())))
    else:
        return (pd.DataFrame(temp_dico_1, columns=list(temp_dico_1.keys())), None)


def create_color(teams, conversion_dico):
    color_dico = {}
    n = len(teams)+1
    for i, team in enumerate(teams):
        color_dico[conversion_dico[team]] = cm.terrain(i/n)
    return color_dico


def create_plots(win, column_name, selected_teams, dico_max, dico_all, df_result, conversion_dico):
    frame_2 = tk.Frame(win)
    frame_2.grid(row=0, column=1)
    colors = create_color(selected_teams, conversion_dico)
    tk.Label(frame_2, text=messages_bundle["dot_inspectors"]).grid(row=0, column=0, sticky="w")
    fig5 = plt.Figure(figsize=(3.5, 2.2), dpi=100)
    plot5 = fig5.add_subplot(111)
    if dico_max[column_name] is not None:
        dataframe, max_dataframe = dico_to_df(dico_all, selected_teams, dico_max[column_name], conversion_dico)
        plot5.bar(max_dataframe[messages_bundle["inspectors"]], max_dataframe["Max"], color=['lightgray']*len(df_result))
    else:
        dataframe = dico_to_df(dico_all, selected_teams, None, conversion_dico)[0]
    c = dataframe[messages_bundle["teams"]].apply(lambda x: colors[x])
    plot5.bar(dataframe[messages_bundle["inspectors"]], dataframe[column_name], color=c)
    for i, j in colors.items():
        plot5.bar(dataframe[messages_bundle["inspectors"]], dataframe[column_name], width=0, color=j, label=i)
    plot5.legend()
    fig5.autofmt_xdate()
    fig5.tight_layout()
    bar5 = FigureCanvasTkAgg(fig5,frame_2)
    bar5.get_tk_widget().grid(row=1, column=0)
    dataframe_2 = dataframe[[messages_bundle["teams"], column_name]].groupby(messages_bundle["teams"]).sum().reset_index()
    c_2 = dataframe_2[messages_bundle["teams"]].apply(lambda x: colors[x])
    tk.Label(frame_2, text=messages_bundle["dot_teams"]).grid(row=2, column=0, sticky="w")
    fig6 = plt.Figure(figsize=(3.5, 2.2), dpi=100)
    plot6 = fig6.add_subplot(111)
    if dico_max[column_name] is not None:
        max_dataframe_2 = max_dataframe[[messages_bundle["teams"], "Max"]].groupby(messages_bundle["teams"]).mean().reset_index()
        plot6.bar(max_dataframe_2[messages_bundle["teams"]], max_dataframe_2["Max"], color=['lightgray']*len(selected_teams))
    plot6.bar(dataframe_2[messages_bundle["teams"]], dataframe_2[column_name], color=c_2)
    fig6.autofmt_xdate()
    fig6.tight_layout()
    bar6 = FigureCanvasTkAgg(fig6,frame_2)
    bar6.get_tk_widget().grid(row=3, column=0)
    return frame_2


def button_stats_command(win, var, dico, selected_teams, dico_max, dico_all, df_result, conversion_dico):
    global frames
    global button_dico
    button_dico['button_stats'].configure(state='disabled')
    frames['plot_stats_frame'].destroy()
    column_name = dico[var.get()]
    frames['plot_stats_frame'] = create_plots(win, column_name, selected_teams, dico_max, dico_all, df_result, conversion_dico)
    win.update()


def statistics(count, tot, df_result, max_charge, selected_teams, conversion_dico):
    global frames
    global messages_bundle
    global button_dico
    root = tk.Toplevel()
    frame = tk.Frame(root)
    frame.grid(row=0, column=0)
    fig1 = Figure(figsize=(1.5, 1), dpi=100)
    plot1 = fig1.add_subplot(111)
    plot1.axis("equal")
    per_1 = round(count/tot, 3)
    plot1.pie([1-per_1, per_1], radius=1.3, startangle=90, colors=["lightgray", get_color(per_1)])
    plot1.pie([1, 0], radius=0.95, startangle=90, colors=["white", "white"])
    plot1.text(0, 0, "{}%".format(round(100*per_1, 1)), horizontalalignment='center', verticalalignment='center', fontsize=10)
    tk.Label(frame, text=messages_bundle["handled_cases"]+": {}".format(count)).grid(row=0, column=0, sticky='e')
    chart1 = FigureCanvasTkAgg(fig1, frame)
    chart1.get_tk_widget().grid(row=0, column=1)
    fig2 = Figure(figsize=(1.5, 1), dpi=100)
    plot2 = fig2.add_subplot(111)
    plot2.axis("equal")
    count_charge, count_max_charge = 0, 0
    mnt_total = 0
    dang_tot = 0
    diff_tot = 0
    prob_tot = 0
    dico_charge = {}
    dico_all = {}
    list_team_charge = []
    list_team_number = []
    for team in selected_teams:
        team_charge = 0
        team_number = 0
        for verif in selected_teams[team]:
            team = str(team)
            charge = df_result['data'][team][str(verif[0])]['charge']
            dico_charge[str(verif[0])] = (charge, len(df_result['data'][team][str(verif[0])]['result']))
            team_charge += dico_charge[str(verif[0])][0]
            #team_number += dico_charge[str(verif[0])][1]/verif[1]
            team_number += df_result['data'][team][str(verif[0])]['nbre']
            count_charge += charge
            count_max_charge += max_charge
            nb, mnt, dang, diff, prob = 0, 0, 0, 0, 0
            for cas in df_result['data'][team][str(verif[0])]['result']:
                nb += 1
                dang_tot += df_result['data'][team][str(verif[0])]['result'][cas]['dangerosity']
                dang += df_result['data'][team][str(verif[0])]['result'][cas]['dangerosity']
                mnt_total += df_result['data'][team][str(verif[0])]['result'][cas]['amount']
                mnt += df_result['data'][team][str(verif[0])]['result'][cas]['amount']
                diff_tot += df_result['data'][team][str(verif[0])]['result'][cas]['difficulty']
                diff += df_result['data'][team][str(verif[0])]['result'][cas]['difficulty']
                prob_tot += df_result['data'][team][str(verif[0])]['result'][cas]['probability']
                prob += df_result['data'][team][str(verif[0])]['result'][cas]['probability']
            if nb > 0:
                dico_all[str(verif[0])] = [nb, mnt, dang, diff, prob/nb, charge]
            else:
                dico_all[str(verif[0])] = [nb, mnt, dang, diff, nb, charge]
            dico_all[str(verif[0])].append(verif[1])
        list_team_charge.append(team_charge)
        list_team_number.append(team_number)
    dico_max = {messages_bundle["teams"]: None, messages_bundle["inspectors"]: None, "Nbr": count, messages_bundle["amount"]: mnt_total, messages_bundle["dangerosity"]: dang_tot, messages_bundle["difficulty"]: diff_tot, messages_bundle["probability"]: None, messages_bundle["charge"]: count_max_charge, "Exp": None}
    per_2 = round(count_charge/count_max_charge, 3)
    plot2.pie([1-min(per_2, 1), min(per_2, 1)], radius=1.3, startangle=90, colors=["lightgray", get_color(per_2)])
    plot2.pie([1, 0], radius=0.95, startangle=90, colors=["white", "white"])
    plot2.text(0, 0, "{}%".format(round(100*per_2, 1)), horizontalalignment='center', verticalalignment='center', fontsize=10)
    tk.Label(frame, text=messages_bundle["used_load"]+":").grid(row=1, column=0, sticky='e')
    chart2 = FigureCanvasTkAgg(fig2, frame)
    chart2.get_tk_widget().grid(row=1, column=1)
    fig3 = Figure(figsize=(1.5, 1), dpi=100)
    plot3 = fig3.add_subplot(111)
    plot3.axis("equal")
    #print(list_team_charge)
    #print(list_team_number)
    ind_charge = np.std(list_team_charge)/np.mean(list_team_charge)
    ind_number = np.std(list_team_number)/np.mean(list_team_number)
    per_3 = max(0, 1-round((ind_charge+ind_number)/2, 3))
    plot3.pie([1-per_3, per_3], radius=1.3, startangle=90, colors=["lightgray", get_color(per_3)])
    plot3.pie([1, 0], radius=0.95, startangle=90, colors=["white", "white"])
    plot3.text(0, 0, "{}%".format(round(100*per_3, 1)), horizontalalignment='center', verticalalignment='center', fontsize=10)
    tk.Label(frame, text=messages_bundle["team_balance"]+":").grid(row=2, column=0, sticky='e')
    chart3 = FigureCanvasTkAgg(fig3, frame)
    chart3.get_tk_widget().grid(row=2, column=1)
    fig4 = Figure(figsize=(1.5, 1), dpi=100)
    plot4 = fig4.add_subplot(111)
    plot4.axis("equal")
    per_2 = round(prob_tot/count, 3)
    plot4.pie([1-per_2, per_2], radius=1.3, startangle=90, colors=["lightgray", get_color(per_2)])
    plot4.pie([1, 0], radius=0.95, startangle=90, colors=["white", "white"])
    plot4.text(0, 0, "{}%".format(round(100*per_2, 1)), horizontalalignment='center', verticalalignment='center', fontsize=10)
    tk.Label(frame, text=messages_bundle["exp_succ"]+":").grid(row=3, column=0, sticky='e')
    chart4 = FigureCanvasTkAgg(fig4, frame)
    chart4.get_tk_widget().grid(row=3, column=1)
    tk.Label(frame, text=messages_bundle["max_rec_amount"]+":").grid(row=4, column=0, sticky='e')
    tk.Label(frame, text=convert_numbers(round(mnt_total, 1), True, lang), font='Helvetica 18 bold').grid(row=4, column=1, sticky='we')
    tk.Label(frame, text=messages_bundle["avg_gain"]+":").grid(row=5, column=0, sticky='e')
    tk.Label(frame, text=convert_numbers(round(mnt_total/count, 1), True, lang), font='Helvetica 18 bold').grid(row=5, column=1, sticky='we')
    tk.Label(frame, text=messages_bundle["avg_diff"]+":").grid(row=6, column=0, sticky='e')
    tk.Label(frame, text=convert_numbers(round(diff_tot/count, 2), False, lang), font='Helvetica 18 bold').grid(row=6, column=1, sticky='we')
    frames['plot_stats_frame'] = create_plots(root, messages_bundle["amount"], selected_teams, dico_max, dico_all, df_result, conversion_dico)
    button_stats_frame = tk.Frame(root)
    button_stats_frame.grid(row=0, column=2)
    tk.Label(button_stats_frame, text=messages_bundle["show_2"]+":").grid(row=0, column=0)
    button_stats_dico = {0: "Nbr", 1: messages_bundle["amount"], 2: messages_bundle["dangerosity"], 3: messages_bundle["difficulty"], 4: messages_bundle["probability"], 5: messages_bundle["charge"], 6: "Exp"}
    varStats = tk.IntVar()
    varStats.set(1)
    varStats.trace('w', lambda *args: button_dico['button_stats'].configure(state='normal'))
    for i in button_stats_dico:
        tk.Radiobutton(button_stats_frame, variable=varStats, text=button_stats_dico[i], value=i).grid(row=i, column=1, sticky='W')
    button_dico['button_stats'] = ttk.Button(button_stats_frame, text=messages_bundle["apply_button"], state='disabled', command=lambda: button_stats_command(root, varStats, button_stats_dico, selected_teams, dico_max, dico_all, df_result, conversion_dico))
    button_dico['button_stats'].grid(row=7, columnspan=2, pady=10)
    root.mainloop()


def _error_frm_widgets(error_frame):
    error_label = ttk.Label(error_frame, anchor=tk.CENTER, text="")
    error_label.grid(row=0, column=0)
    return error_label


def _dashboard_frm_widgets(dashboard_frame, error_label, team_list, conversion_dico, database):
    global to_reset
    global to_disable
    global current_state
    global sort_etiqs
    global algo_etiqs
    left_dashboard_frame = tk.Frame(dashboard_frame, background="#f1f1f1")
    left_dashboard_frame.grid(row=0, column=0)
    upper_bound = len(database)
    ttk.Label(left_dashboard_frame, text=messages_bundle["threshold"]+":").grid(row=1, column=0, padx=10, sticky='W')
    to_reset['varCase'] = (tk.DoubleVar(), 's')
    to_reset['varCase'][0].set(100)
    to_disable['case_number'] = (tk.Scale(left_dashboard_frame, from_=0, to=100, orient='horizontal', variable=to_reset['varCase'][0], background='#f1f1f1', tickinterval=20, length=200), 0)
    to_reset['varCase'][0].trace('w', lambda *args: check_int(to_reset['varCase'][0], to_disable['case_number'][0], error_label, 1, *args))
    to_disable['case_number'][0].grid(row=1, column=1)
    ttk.Label(left_dashboard_frame, text=messages_bundle["sort_met"]+":").grid(row=3, column=0, padx=10, sticky='W')
    sort_vals = [0, 1, 2, 3]
    sort_etiqs = [messages_bundle["dangerosity"], messages_bundle["probability"], messages_bundle["dangerosity_index"],messages_bundle["amount"]]
    to_reset['varSort'] = (tk.IntVar(), 'i')
    to_reset['varSort'][0].set(sort_vals[0])
    to_reset['varSort'][0].trace('w', lambda *args: trace_function())
    for i in range(len(sort_vals)):
        to_disable['sort_{}'.format(i)] = (tk.Radiobutton(left_dashboard_frame, variable=to_reset['varSort'][0], text=sort_etiqs[i], value=sort_vals[i], background="#f1f1f1"), 0)
        to_disable['sort_{}'.format(i)][0].grid(row=3+i, column=1, sticky='W')
    ttk.Label(left_dashboard_frame, text=messages_bundle["algorithm"]+":").grid(row=3+len(sort_vals), column=0, padx=10, sticky='W')
    algo_vals = [0, 1, 2]
    algo_etiqs = [messages_bundle["full_met"], messages_bundle["approx_met"], messages_bundle['efficient_appr']]
    to_reset['varAlgo'] = (tk.IntVar(), 'i')
    to_reset['varAlgo'][0].set(sort_vals[0])
    to_reset['varAlgo'][0].trace('w', lambda *args: trace_function())
    for i in range(len(algo_vals)):
        to_disable['algo_{}'.format(i)] = (tk.Radiobutton(left_dashboard_frame, variable=to_reset['varAlgo'][0], text=algo_etiqs[i], value=algo_vals[i], background="#f1f1f1"), 0)
        to_disable['algo_{}'.format(i)][0].grid(row=3+len(sort_vals)+i, column=1, sticky='W')
    ttk.Label(left_dashboard_frame, text=messages_bundle["max_load"]+":").grid(row=3+len(sort_vals)+len(algo_vals), column=0, padx=10, sticky='W')
    to_reset['varCharge'] = (tk.StringVar(), 's')
    to_disable['charge_spin'] = (tk.Spinbox(left_dashboard_frame, from_=0, to=10000000, increment=1, state='normal', textvariable=to_reset['varCharge'][0], highlightbackground="#f1f1f1", buttonbackground="#f1f1f1"), 0)
    to_reset['varCharge'][0].trace('w', lambda *args: check_int(to_reset['varCharge'][0], to_disable['charge_spin'][0], error_label, 2, *args))
    to_disable['charge_spin'][0].grid(row=3+len(sort_vals)+len(algo_vals), column=1)
    if lang=="fr":
        ttk.Label(left_dashboard_frame, text="Il y a {} cas à traiter".format(upper_bound)).grid(row=2, column=1, sticky='we')
    else:
        ttk.Label(left_dashboard_frame, text="There are {} cases to handle".format(upper_bound)).grid(row=2, column=1, sticky='we')
    right_dashboard_frame = tk.Frame(dashboard_frame, background="#f1f1f1")
    right_dashboard_frame.grid(row=0, column=1)
    ttk.Label(right_dashboard_frame, text=messages_bundle["select_teams"]+":").grid(row=1, column=2, sticky='W')
    if team_list is None:
        current_state['team'] = BLC_ERROR
        update_state(error_label)
        disable()
    elif len(team_list) == 0:
        current_state['team'] = LST_ERROR
        update_state(error_label)
        disable()
    else:
        to_reset['all_name'] = (tk.IntVar(), 'i')
        to_reset['all_name'][0].trace('w', lambda *args: trace_function())
        to_disable['all_checkbox'] = (tk.Checkbutton(right_dashboard_frame, text="", variable=to_reset['all_name'][0], onvalue=1, offvalue=0, background="#f1f1f1",  command=lambda:check_all(to_reset['all_name'][0], team_dico)), 1)
        to_disable['all_checkbox'][0].grid(row=1, column=4, sticky='W')
        for i in range(len(team_list)):
            team_name = team_list[i]
            converted_team_name = conversion_dico[team_name]
            if converted_team_name in team_cat:
                if len(team_cat[converted_team_name]) > 0:
                    converted_team_name += " ({})".format(team_cat[converted_team_name])
            to_reset['name_check_{}'.format(i)] = (tk.IntVar(), 'i')
            to_reset['name_check_{}'.format(i)][0].trace('w', lambda *args: check_indiv(team_dico, to_disable['all_checkbox'][0], error_label))
            to_disable['name_checkbox_{}'.format(i)] = (tk.Checkbutton(right_dashboard_frame, text=converted_team_name, variable=to_reset['name_check_{}'.format(i)][0], background="#f1f1f1", onvalue=1, offvalue=0), 1)
            to_disable['name_checkbox_{}'.format(i)][0].grid(row=2+i, column=4, sticky='W')
            team_dico[team_name] = (to_reset['name_check_{}'.format(i)][0], to_disable['name_checkbox_{}'.format(i)][0])
        current_state['team'] = MBM_ERROR
        update_state(error_label)
    if upper_bound == 0:
        current_state[1] = BND_ERROR
        update_state(error_label)
        disable()


def _button_frm_widget(button_frame, equipes, conversion_dico, database):
    global messages_bundle
    global button_dico
    global to_reset
    global frames
    button_dico['cancel'] = ttk.Button(button_frame, text=messages_bundle["clear_button"], state='disabled', command=cancel)
    button_dico['cancel'].grid(row=0, column=0, sticky='we', padx=10, pady=10)
    button_dico['reset'] = ttk.Button(button_frame, text=messages_bundle["reset_button"], state='disabled', command=reset)
    button_dico['reset'].grid(row=0, column=1, sticky='we', padx=10, pady=10)
    button_dico['apply'] = ttk.Button(button_frame, text=messages_bundle["apply_button"], state='disabled', command=lambda: apply(frames['current'], frames['window'], database, equipes, conversion_dico))
    button_dico['apply'].grid(row=0, column=2, sticky='we', padx=10, pady=10)
    to_reset['error'] = (ttk.Label(button_frame, foreground='red', text=""), 'l', 0)
    to_reset['error'][0].grid(row=1, columnspan=3)


def convert_res():
    cursor.execute("SELECT username, experience, team_name FROM Members")
    res_list = cursor.fetchall()
    team_to_convert = {}
    for elem in res_list:
        if elem[2] not in team_to_convert:
            team_to_convert[elem[2]] = [(elem[0], elem[1])]
        else:
            team_to_convert[elem[2]].append((elem[0], elem[1]))
    return team_to_convert


def init_global():
    global CANCEL_ACTIVE
    global DISABLED_ACTIVE
    global current_state
    global error_dico
    global to_disable
    global to_reset
    global button_dico
    global frames
    global team_dico
    global messages_bundle
    global sort_etiqs
    global algo_etiqs
    global lang
    global cursor
    global conn
    global team_cat

    CANCEL_ACTIVE = False
    DISABLED_ACTIVE = False
    current_state = {'global': NO_ERROR, 1: NBR_ERROR, 2: NBR_ERROR, 'team': NO_ERROR}
    error_dico = {}
    to_disable = {}
    to_reset = {}
    button_dico = {}
    frames = {}
    team_dico = {}
    messages_bundle = {}
    sort_etiqs = []
    algo_etiqs = []
    lang = None
    cursor = None
    conn = None
    team_cat = {}


def add_widgets(window, affect_tab, messages_bundles, cursors, conns, langs):
    global messages_bundle
    global cursor
    global conn
    global lang
    global frames
    global error_dico
    global CANCEL_ACTIVE
    global team_cat

    init_global()

    messages_bundle = messages_bundles
    cursor = cursors
    conn = conns
    lang = langs
    error_dico = {0: ('black', ""), 1: ('red', messages_bundle["positive_number"]), 2: ('red', messages_bundle["choose_teams"]), 3: ('red', messages_bundle["must_teams"]), 4: ('red', messages_bundle["must_data"]), 5: ('red', messages_bundle["must_balance"])}

    cursor.execute("SELECT team_name, speciality FROM Teams")
    res2 = cursor.fetchall()
    team_cat = {elem[0]: elem[1] for elem in res2}
    team_to_convert = convert_res()
    frames['window'] = window
    frames['current'] = affect_tab
    config.configure_style()
    error_frame = tk.Frame(frames['current'], background="#f1f1f1")
    error_label = _error_frm_widgets(error_frame)
    update_state(error_label)
    error_frame.pack()
    data_to_consider = get_data()
    equipes, conversion_dico = convert_teams(team_to_convert)
    if equipes is None:
        team_list = None
    else:
        team_list = list(equipes.keys())
    dashboard_frame = tk.Frame(frames['current'], background="#f1f1f1")
    _dashboard_frm_widgets(dashboard_frame, error_label, team_list, conversion_dico, data_to_consider)
    dashboard_frame.pack()
    button_frame = tk.Frame(frames['current'], background="#f1f1f1")
    _button_frm_widget(button_frame, equipes, conversion_dico, data_to_consider)
    button_frame.pack()
    save_file = open("./data/data.json", "r")
    res_dict = json.load(save_file)
    save_file.close()
    if len(res_dict) > 0 and not DISABLED_ACTIVE:
        result_frame = tk.Frame(frames['current'], background="#f1f1f1")
        plot_affect_from_save_dico(result_frame, res_dict, conversion_dico)
        result_frame.pack()
        frames['result'] = result_frame
        button_dico['cancel'].configure(state='normal')
        CANCEL_ACTIVE = True
    if len(res_dict) == 0:
        CANCEL_ACTIVE = False
