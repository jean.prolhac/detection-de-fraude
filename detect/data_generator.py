import numpy as np
import pandas as pd


parameters_df = pd.read_csv("./detect/parameters/distribution_parameters.csv")
pourcentage = {parameters_df.iloc[i]['SPC']: parameters_df.iloc[i]['SPC Ratio'] for i in range(len(parameters_df))}
expense = {parameters_df.iloc[i]['SPC']: parameters_df.iloc[i]['Expenses Ratio'] for i in range(len(parameters_df))}
categories = list(parameters_df['SPC'].to_numpy())


def burr_distribution(k, c, l, num):
    """
    Returns an num-long array of elements generated thanks to Singh-Maddala law with parameters k, c, l

    Parameters
    __________
    k, c, l: float
    num: int

    Returns
    _______
    numpy.array
    """
    def inv_burr_cdf(k, c, l, y):
        return l*((1-y)**(-1/k)-1)**(1/c)
    return (lambda x: inv_burr_cdf(k, c, l, x))(np.random.uniform(0, 1, (1, num)))[0]


def generate(n, progress=None, norm_coeff=None):
    """
    Generates a sample of n fiscal cases to study

    Parameters
    __________
    n: int
    progress: dict
    norm_coeff: float

    Returns
    _______
    list tuple
    """
    global categories
    global parameters_df
    global pourcentage
    categorie = []
    count = []
    m = []
    p = []
    for index, x in enumerate(categories):
        a = round(pourcentage[x]*n)
        categorie += a*[categories.index(x)]
        b = burr_distribution(parameters_df.iloc[index]['Wage k'],
                              parameters_df.iloc[index]['Wage c'],
                              parameters_df.iloc[index]['Wage l'],
                              a)
        count += list(b)
        m += [expense[x]*w for w in b]
        p += list(burr_distribution(parameters_df.iloc[index]['Estate k'],
                                    parameters_df.iloc[index]['Estate c'],
                                    parameters_df.iloc[index]['Estate l'],
                                    a))
        progress_value = int(norm_coeff*(20*index/len(categories)))
        progress[0]['value'] = progress_value
        progress[1].configure(text="{}%".format(progress_value))
        progress[2].update()
    a = len(count)
    noise = np.random.normal(0, 3000, a)
    wage = []
    for i in range(a):
        wage.append(noise[i]+count[i])
        progress_value = int(norm_coeff*(20+20*i/a))
        progress[0]['value'] = progress_value
        progress[1].configure(text="{}%".format(progress_value))
        progress[2].update()
    noise = np.random.normal(0, 1000, a)
    expenses = []
    for i in range(a):
        expenses.append(m[i]+noise[i])
        progress_value = int(norm_coeff*(40+20*i/a))
        progress[0]['value'] = progress_value
        progress[1].configure(text="{}%".format(progress_value))
        progress[2].update()
    noise = np.random.normal(0, 10000, a)
    estate = []
    for i in range(a):
        estate.append(p[i] + noise[i])
        progress_value = int(norm_coeff*(60+20*i/a))
        progress[0]['value'] = progress_value
        progress[1].configure(text="{}%".format(progress_value))
        progress[2].update()
    return categorie, wage, expenses, estate


def save_data(n, progress=None, norm_coeff=None):
    """
    Generates and saves a sample of n fiscal cases to study

    Parameters
    __________
    n: int
    progress: dict
    norm_coeff: float

    Returns
    _______
    None
    """
    cat, sal, ex, pat = generate(n, progress, norm_coeff)
    df = pd.DataFrame(list(zip(cat, sal, ex, pat)),
                      columns=["cat", "wage", "expenses", "estate"])
    df.index.name = "id"
    df.to_csv('./data/generated_data.tsv', sep='\t')
    progress_value = int(100*norm_coeff)
    progress[0]['value'] = progress_value
    progress[1].configure(text="{}%".format(progress_value))
    progress[2].update()

#save_data(200000)
