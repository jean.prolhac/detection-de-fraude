import pandas as pd
import numpy as np
import tkinter as tk
from tkinter import ttk
from tensorflow import keras
import pickle
from PIL import Image, ImageTk
from matplotlib.figure import Figure
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from  matplotlib.colors import LinearSegmentedColormap
import json
import joblib
import sys
sys.path.append('.')
from detect.data_generator import save_data
import gui.gui_config as config


global model_dico
global scaler

model_dico = {'model': [True], 'weighted_model': [True], 'resampled_model': [True], 'random_forest_precision': [False], 'random_forest_recall': [False]}
scaler = pickle.load(open('./detect/parameters/scaler.pkl', 'rb'))
for model_type in model_dico:
    if model_dico[model_type][0]:
        model_dico[model_type].append(keras.models.load_model('./detect/parameters/{}'.format(model_type)))
    else:
        model_dico[model_type].append(joblib.load('./detect/parameters/{}.pkl'.format(model_type)))


def inference(type_model, progress, norm_coeff, ord_coeff, threshold):
    global model_dico
    global scaler
    database = pd.read_csv("./data/generated_data.tsv", sep="\t")
    to_save_df = database.copy()
    to_save_df = to_save_df[['cat', 'wage', 'expenses', 'estate']]
    to_process_df = database.copy()
    to_process_df = to_process_df[['cat', 'wage', 'expenses', 'estate']]
    to_process_df = np.array(to_process_df)
    if model_dico[type_model][0]:
        to_predict = scaler.transform(to_process_df)
        result = model_dico[type_model][1].predict(to_predict)
    else:
        to_predict = pd.DataFrame(to_process_df, columns=['cat', 'wage', 'Log expenses', 'Log estate'])
        result = model_dico[type_model][1].predict_proba(to_predict)[:,1]
    progress_value = int(ord_coeff+norm_coeff*100)
    progress[0]['value'] = progress_value
    progress[1].configure(text="{}%".format(progress_value))
    progress[2].update()
    to_save_df['result_{}+{}'.format(type_model, threshold)] = result
    to_save_df.index.name = 'id'
    to_save_df.to_csv('./data/generated_data.tsv', sep='\t')


NO_ERROR = 0
NBR_ERROR = 1
ABL_ERROR = 2
EPY_ERROR = 3
current_state = {'global': NO_ERROR, 'gene': NBR_ERROR, 'eval': NO_ERROR}
gene_dico = {}
eval_dico = {}
frames = {}
param_perf_dico = {}
check_dico = {}
button_dico = {}
convert_cat_dico = {}
convert_model_dico = {}
type_etiqs = ["model", "weighted_model", "resampled_model", "random_forest_precision", "random_forest_recall"]
messages_bundle = {}
lang = None
cursor = None
conn = None
performances_df = pd.read_excel("./detect/results/Performances.xlsx").set_index('Modèle')


def check_int(var, entry, lbl):
    global current_state
    number = var.get()
    if not number.isdigit() or number == '0':
        entry.configure(highlightbackground="red")
        lbl.configure(text=messages_bundle['positive_number'])
        current_state['gene'] = NBR_ERROR
        button_dico['apply_button'].configure(state='disabled')
        if current_state['global'] == EPY_ERROR:
            check_dico['check_eval'].configure(state='disabled')
            check_dico['check_eval'].deselect()
    else:
        current_state['gene'] = NO_ERROR
        button_dico['apply_button'].configure(state='normal')
        entry.configure(highlightbackground="#f1f1f1")
        check_dico['check_eval'].configure(state='normal')
        if current_state['global'] == EPY_ERROR:
            lbl.configure(text=messages_bundle['must_data_2'])
        else:
            lbl.configure(text="")


def trace_function(var, other_var, dico, is_gene):
    global current_state
    global button_dico
    is_checked = var.get()
    if is_checked == 1:
        button_dico['reset_button'].configure(state='normal')
        for widget_name in dico:
            dico[widget_name].configure(state='normal')
        if is_gene:
            if current_state['gene'] == NO_ERROR:
                button_dico['apply_button'].configure(state='normal')
            else:
                button_dico['apply_button'].configure(state='normal')
        else:
            button_dico['apply_button'].configure(state='normal')
    else:
        for widget_name in dico:
            dico[widget_name].configure(state='disabled')
        if is_gene:
            if current_state['global'] == EPY_ERROR:
                button_dico['apply_button'].configure(state='disabled')
                check_dico['check_eval'].configure(state='disabled')
                check_dico['check_eval'].deselect()
        if current_state['global'] == NO_ERROR:
            is_other_checked = other_var.get()
            if is_other_checked == 1:
                button_dico['apply_button'].configure(state='normal')
            else:
                button_dico['apply_button'].configure(state='disabled')
                button_dico['reset_button'].configure(state='disabled')


def trace_perf():
    global param_perf_dico
    global type_etiqs
    global convert_model_dico
    new_val = type_etiqs[convert_model_dico[param_perf_dico['varModPerf'].get()]]
    if not new_val == param_perf_dico['current_varModPerf'][0]:
        param_perf_dico['apply_but_perf'].configure(state='normal')
        param_perf_dico['cancel_but_perf'].configure(state='normal')
        param_perf_dico['current_varModPerf'][1] = False
    else:
        param_perf_dico['current_varModPerf'][1] = True
        param_perf_dico['apply_but_perf'].configure(state='disabled')
        param_perf_dico['cancel_but_perf'].configure(state='disabled')


def plot_perf(win, lbl_conf, lbl_auprc, lbl_roc, mdl_name, lbl_perf_0, lbl_perf_1, lbl_perf_2, lbl_perf_3):
    con_matrix = Image.open('./detect/results/confusion_{}.png'.format(mdl_name)).resize((310, 310), Image.ANTIALIAS)
    display_conf_matrix = ImageTk.PhotoImage(con_matrix)
    lbl_conf.configure(image=display_conf_matrix)
    lbl_conf.image = display_conf_matrix
    lbl_perf_0.configure(text=convert_numbers(float(round(performances_df[[mdl_name]].loc['PRC'], 3)), False, lang))
    lbl_perf_1.configure(text=convert_numbers(float(round(performances_df[[mdl_name]].loc['AUC'], 3)), False, lang))
    lbl_perf_2.configure(text=convert_numbers(float(round(performances_df[[mdl_name]].loc['Precision'], 3)), False, lang))
    lbl_perf_3.configure(text=convert_numbers(float(round(performances_df[[mdl_name]].loc['Recall'], 3)), False, lang))
    auprc = Image.open('./detect/results/PRC.png').resize((500, 500), Image.ANTIALIAS)
    w1, h1 = auprc.size
    display_auprc = ImageTk.PhotoImage(auprc.crop((50, 0, w1-50, h1)))
    lbl_auprc.configure(image=display_auprc)
    lbl_auprc.image = display_auprc
    roc = Image.open('./detect/results/ROC.png').resize((500, 500), Image.ANTIALIAS)
    w2, h2 = roc.size
    display_roc = ImageTk.PhotoImage(roc.crop((50, 0, w2-50, h2)))
    lbl_roc.configure(image=display_roc)
    lbl_roc.image = display_roc
    win.update()


def apply_perf(win, lbl_conf, lbl_auprc, lbl_roc, lbl_perf_0, lbl_perf_1, lbl_perf_2, lbl_perf_3):
    global type_etiqs
    global convert_model_dico
    global param_perf_dico
    param_perf_dico['apply_but_perf'].configure(state='disabled')
    param_perf_dico['cancel_but_perf'].configure(state='disabled')
    mdl_name = type_etiqs[convert_model_dico[param_perf_dico['varModPerf'].get()]]
    param_perf_dico['current_varModPerf'] = [param_perf_dico['varModPerf'].get(), True]
    plot_perf(win, lbl_conf, lbl_auprc, lbl_roc, mdl_name, lbl_perf_0, lbl_perf_1, lbl_perf_2, lbl_perf_3)


def cancel_perf():
    global param_perf_dico
    param_perf_dico['cancel_but_perf'].configure(state='disabled')
    param_perf_dico['apply_but_perf'].configure(state='disabled')
    param_perf_dico['varCat'].set(param_perf_dico['current_varCat'][0])
    param_perf_dico['current_varCat'][1] = True
    param_perf_dico['varModPerf'].set(param_perf_dico['current_varModPerf'][0])
    param_perf_dico['current_varModPerf'][1] = True


def convert_numbers(number, long_nbr, lang):
    if lang=="fr":
        if long_nbr:
            return "{:,.2f}".format(number).replace(',', ' ').replace('.', ',')
        else:
            return "{}".format(number).replace('.', ',')
    else:
        if long_nbr:
            return "{:,.2f}".format(number)
        else:
            return str(number)


def performances():
    global param_perf_dico
    global convert_cat_dico
    global convert_model_dico
    global type_etiqs
    root = tk.Toplevel()
    param_frame = tk.Frame(root)
    tk.Label(param_frame, text=messages_bundle['model_type']+":").grid(row=0, column=0, sticky='e')
    mod_list = list(convert_model_dico.keys())
    param_perf_dico['varModPerf'] = tk.StringVar()
    param_perf_dico['current_varModPerf'] = [mod_list[0], True]
    param_perf_dico['varModPerf'].set(param_perf_dico['current_varModPerf'][0])
    param_perf_dico['choose_mod'] = tk.OptionMenu(param_frame, param_perf_dico['varModPerf'], *mod_list)
    param_perf_dico['choose_mod'].config(width=15)
    param_perf_dico['choose_mod'].grid(row=0, column=1, sticky='we')
    param_frame.grid(row=0, column=0, columnspan=2)
    left_frame = tk.Frame(root)
    model_selected = type_etiqs[convert_model_dico[param_perf_dico['current_varModPerf'][0]]]
    tk.Label(left_frame, text=messages_bundle['conf_mat']+":").grid(row=5, column=0, sticky='w', padx=10)
    con_matrix = Image.open('./detect/results/confusion_{}.png'.format(model_selected)).resize((310, 310), Image.ANTIALIAS)
    display_conf_matrix = ImageTk.PhotoImage(con_matrix)
    label_conf_matrix = tk.Label(left_frame, image=display_conf_matrix)
    label_conf_matrix.grid(row=6, column=0, columnspan=2)
    sub_left_frame = tk.Frame(left_frame)
    tk.Label(sub_left_frame, text=messages_bundle['auprc']+":").grid(row=0, column=0, sticky='w', padx=10)
    tk.Label(sub_left_frame, text=messages_bundle['auroc']+":").grid(row=1, column=0, sticky='w', padx=10)
    tk.Label(sub_left_frame, text=messages_bundle['precision']+":").grid(row=2, column=0, sticky='w', padx=10)
    tk.Label(sub_left_frame, text=messages_bundle['recall']+":").grid(row=3, column=0, sticky='w', padx=10)
    lbl_perf_0 = tk.Label(sub_left_frame, text=convert_numbers(float(round(performances_df[[model_selected]].loc['PRC'], 3)), False, lang))
    lbl_perf_0.grid(row=0, column=1, sticky='w', padx=10)
    lbl_perf_1 = tk.Label(sub_left_frame, text=convert_numbers(float(round(performances_df[[model_selected]].loc['AUC'], 3)), False, lang))
    lbl_perf_1.grid(row=1, column=1, sticky='w', padx=10)
    lbl_perf_2 = tk.Label(sub_left_frame, text=convert_numbers(float(round(performances_df[[model_selected]].loc['Precision'], 3)), False, lang))
    lbl_perf_2.grid(row=2, column=1, sticky='w', padx=10)
    lbl_perf_3 = tk.Label(sub_left_frame, text=convert_numbers(float(round(performances_df[[model_selected]].loc['Recall'], 3)), False, lang))
    lbl_perf_3.grid(row=3, column=1, sticky='w', padx=10)
    sub_left_frame.grid(row=7, column=0, sticky='w', pady=10)
    right_frame = tk.Frame(root)
    tk.Label(right_frame, text=messages_bundle['prc']+":").grid(row=0, column=0, sticky='nw')
    auprc = Image.open('./detect/results/PRC.png').resize((500, 500), Image.ANTIALIAS)
    w1, h1 = auprc.size
    display_auprc = ImageTk.PhotoImage(auprc.crop((50, 0, w1-50, h1)))
    label_auprc = tk.Label(right_frame, image=display_auprc)
    label_auprc.grid(row=1, column=0, sticky='n')
    tk.Label(right_frame, text=messages_bundle['roc']+":").grid(row=0, column=1, sticky='nw')
    roc = Image.open('./detect/results/ROC.png').resize((500, 500), Image.ANTIALIAS)
    w2, h2 = roc.size
    display_roc = ImageTk.PhotoImage(roc.crop((50, 0, w2-50, h2)))
    label_roc = tk.Label(right_frame, image=display_roc)
    label_roc.grid(row=1, column=1, sticky='n')
    param_perf_dico['cancel_but_perf'] = ttk.Button(param_frame, text=messages_bundle["cancel_button"], state='disabled', command=lambda: cancel_perf())
    param_perf_dico['cancel_but_perf'].grid(row=2, column=0, padx=10, pady=10)
    param_perf_dico['apply_but_perf'] = ttk.Button(param_frame, text=messages_bundle['apply_button'], state='disabled', command=lambda: apply_perf(root, label_conf_matrix, label_auprc, label_roc, lbl_perf_0, lbl_perf_1, lbl_perf_2, lbl_perf_3))
    param_perf_dico['apply_but_perf'].grid(row=2, column=1, padx=10, pady=10)
    param_perf_dico['varModPerf'].trace('w', lambda *args: trace_perf())
    right_frame.grid(row=1, column=1, sticky='n')
    left_frame.grid(row=1, column=0, sticky='n')
    root.mainloop()


def plot_show(root, wage_data, expenses_data, estate_data, prob_data, name, threshold=0.5):
    frame = tk.Frame(root)
    tk.Label(frame, text=messages_bundle['currently']+": {}".format(name), font='Helvetica 14 bold').grid(row=0, column=0, columnspan=2)
    tk.Label(frame, text=messages_bundle['case_nbr']+": {}".format(len(wage_data))).grid(row=1, column=0, sticky='w')
    tk.Label(frame, text=messages_bundle['fraud_nbr']+": {}".format(len(prob_data[prob_data[:] > threshold]))).grid(row=1, column=1, sticky='w')
    crg = LinearSegmentedColormap.from_list('rg', [(0, "limegreen"), (0.25, "gold"), (0.5, "orange"), (1, "red")], N=256)
    fig1 = Figure(figsize=(6, 6), dpi=100)
    plot1 = fig1.add_subplot(111)
    plot1.set_xlabel(messages_bundle['wage'])
    plot1.set_ylabel(messages_bundle['estate'])
    mp1 = plot1.scatter(wage_data, estate_data, c=prob_data, cmap=crg, vmin=0, vmax=1)
    plot1.ticklabel_format(axis="y", style="sci", scilimits=(0,0))
    fig1.colorbar(mp1)
    fig1.autofmt_xdate()
    scatter1 = FigureCanvasTkAgg(fig1, frame)
    scatter1.get_tk_widget().grid(row=2, column=0)
    fig2 = Figure(figsize=(6, 6), dpi=100)
    plot2 = fig2.add_subplot(111)
    plot2.set_xlabel(messages_bundle['wage'])
    plot2.set_ylabel(messages_bundle['expenses'])
    mp2 = plot2.scatter(wage_data, expenses_data, c=prob_data, cmap=crg, vmin=0, vmax=1)
    plot2.ticklabel_format(axis="y", style="sci", scilimits=(0,0))
    fig2.colorbar(mp2)
    fig2.autofmt_xdate()
    scatter2 = FigureCanvasTkAgg(fig2, frame)
    scatter2.get_tk_widget().grid(row=2, column=1, padx=10)
    frame.pack()
    return frame


def apply_plot(root, var, to_display):
    global convert_cat_dico
    wanted = var.get()
    if wanted == messages_bundle['all_2']:
        estate_data = to_display[:, 4]
        wage_data = to_display[:, 2]
        expenses_data = to_display[:, 3]
        prob_data = to_display[:, 5]
    else:
        estate_data = to_display[to_display[:, 1]==convert_cat_dico[wanted], 4]
        wage_data = to_display[to_display[:, 1]==convert_cat_dico[wanted], 2]
        expenses_data = to_display[to_display[:, 1]==convert_cat_dico[wanted], 3]
        prob_data = to_display[to_display[:, 1]==convert_cat_dico[wanted], 5]
    if not frames['plot'] is None:
        frames['plot'].destroy()
    frames['plot'] = plot_show(root, wage_data, expenses_data, estate_data, prob_data, wanted)
    root.update()


def show_data(to_display, threshold=0.5):
    global convert_cat_dico
    cat_lit = [messages_bundle['all_2']]
    cat_lit.extend(list(convert_cat_dico.keys()))
    root = tk.Toplevel()
    param_frm = tk.Frame(root)
    tk.Label(param_frm, text=messages_bundle['show_2']+":").grid(row=0, column=0, sticky='e')
    varPlot = tk.StringVar()
    varPlot.set(cat_lit[0])
    choose_plot = tk.OptionMenu(param_frm, varPlot, *cat_lit)
    choose_plot.config(width=17)
    choose_plot.grid(row=0, column=1)
    ttk.Button(param_frm, text=messages_bundle['apply_button'], state='normal', command=lambda: apply_plot(root, varPlot, to_display)).grid(row=0, column=2, padx=10, pady=10)
    param_frm.pack()
    frames['plot'] = plot_show(root, to_display[:, 2], to_display[:, 3], to_display[:, 4], to_display[:, 5], messages_bundle['all_2'], threshold)
    root.mainloop()


def display_data(win, database, is_result):
    display_frame = tk.Frame(win, background="#f1f1f1")
    to_display = database.to_numpy()
    ttk.Label(display_frame, text=messages_bundle['case_nbr']+": {}".format(len(to_display))).grid(row=0, column=0, pady=5, sticky='w')
    if not is_result:
        treeview = ttk.Treeview(display_frame, columns=('Id', 'Catégorie', 'Salaire', 'Dépenses', 'Patrimoine'))
        treeview.column('#0', width=0, stretch = "no")
        treeview.column('Id', anchor='center')
        treeview.column('Catégorie', width=65, anchor='center')
        treeview.column('Salaire', anchor='center')
        treeview.column('Dépenses', anchor='center')
        treeview.column('Patrimoine', anchor='center')
        treeview.heading('Id', text=messages_bundle['id'])
        treeview.heading('Catégorie', text=messages_bundle['cat'])
        treeview.heading('Salaire', text=messages_bundle['wage'])
        treeview.heading('Dépenses', text=messages_bundle['expenses'])
        treeview.heading('Patrimoine', text=messages_bundle['estate'])
        for case in to_display:
            id, cat, wage, expenses, estate = case
            item_name = 'item_{}'.format(int(id))
            treeview.insert('', 'end', item_name, text="", values=(int(id), int(cat), convert_numbers(round(wage, 2), True, lang), convert_numbers(round(expenses, 2), True, lang), convert_numbers(round(estate, 2), True, lang)))
    else:
        column_model_name, threshold = list(database.columns)[-1].split('+')
        ttk.Label(display_frame, text=messages_bundle['used_method']+": {}".format(messages_bundle[column_model_name[7:]])).grid(row=0, column=1, pady=5, sticky='w')
        treeview = ttk.Treeview(display_frame, columns=('Id', 'Catégorie', 'Salaire', 'Dépenses', 'Patrimoine', 'Probabilité'))
        treeview.column('#0', width=0, stretch = "no")
        treeview.column('Id', anchor='center')
        treeview.column('Catégorie', width=65, anchor='center')
        treeview.column('Salaire', anchor='center')
        treeview.column('Dépenses', anchor='center')
        treeview.column('Patrimoine', anchor='center')
        treeview.column('Probabilité', anchor='center')
        treeview.heading('Id', text=messages_bundle['id'])
        treeview.heading('Catégorie', text=messages_bundle['cat'])
        treeview.heading('Salaire', text=messages_bundle['wage'])
        treeview.heading('Dépenses', text=messages_bundle['expenses'])
        treeview.heading('Patrimoine', text=messages_bundle['estate'])
        treeview.heading('Probabilité', text=messages_bundle['probability'])
        count_prob = 0
        for case in to_display:
            id, cat, wage, expenses, estate, proba = case
            if proba > float(threshold):
                count_prob += 1
            item_name = 'item_{}'.format(int(id))
            treeview.insert('', 'end', item_name, text="", values=(int(id), int(cat), convert_numbers(round(wage, 2), True, lang), convert_numbers(round(expenses, 2), True, lang), convert_numbers(round(estate, 2), True, lang), convert_numbers(round(proba, 3), False, lang)))
        ttk.Label(display_frame, text=messages_bundle['fraud_rate']+": {}% ({}: {})".format(convert_numbers(round(100*count_prob/len(to_display), 1), False, lang), messages_bundle['threshold'], int(100*float(threshold)))).grid(row=2, column=0, pady=10, sticky='we')
        ttk.Button(display_frame, text=messages_bundle['show'], state='normal', command=lambda: show_data(to_display, float(threshold))).grid(row=2, column=1, pady=10, sticky='w')
    treeview.grid(row=1, column=0, columnspan=2)
    scrollbar = ttk.Scrollbar(display_frame, orient=tk.VERTICAL, command=treeview.yview)
    treeview.configure(yscroll=scrollbar.set)
    scrollbar.grid(row=1, column=2, sticky='ns')
    display_frame.pack()
    win.update()
    return display_frame


def apply(current, win, error_lbl):
    global current_state
    global check_dico
    global type_etiqs
    global button_dico
    global frames
    button_dico['apply_button'].configure(state='disabled')
    gene = check_dico['varGene'].get()
    eval = check_dico['varEval'].get()
    frm = frames['display']
    if frm is not None:
        frm.destroy()
    progress_frame = tk.Frame(current, background="#f1f1f1")
    ttk.Label(progress_frame, text="").pack()
    progress_bar = ttk.Progressbar(progress_frame, orient='horizontal', mode='determinate', length=280)
    progress_bar.pack()
    progress_label = ttk.Label(progress_frame, text="", anchor=tk.CENTER)
    progress_label.pack()
    progress_frame.pack()
    progress = (progress_bar, progress_label, win)
    if gene and eval:
        norm_coeff = 0.5
        ord_coeff = 50
    else:
        norm_coeff = 1
        ord_coeff = 0
    if gene:
        nbr_to_gene = int(check_dico['varNbr'].get())
        save_data(nbr_to_gene, progress, norm_coeff)
    if eval:
        type_mdl = type_etiqs[check_dico['varVals'].get()]
        threshold = int(check_dico['varThreshold'].get())/100
        inference(type_mdl, progress, norm_coeff, ord_coeff, threshold)
        is_result = True
    else:
        is_result = False
    progress_frame.destroy()
    database = pd.read_csv("./data/generated_data.tsv", sep="\t")
    frames['display'] = display_data(current, database, is_result)
    current_state['global'] = NO_ERROR
    error_lbl.configure(text="")
    reset()
    save_file = open("./affectations/data.json", "w")
    json.dump({}, save_file)
    save_file.close()


def reset():
    global check_dico
    global button_dico
    button_dico['reset_button'].configure(state='disabled')
    check_dico['varNbr'].set(0)
    check_dico['check_gene'].deselect()
    check_dico['varVals'].set(0)
    check_dico['check_eval'].deselect()
    check_dico['varThreshold'].set(50)


def add_widgets(window, affect_tab, messages_bundles, cursors, conns, langs):
    global messages_bundle
    global cursor
    global conn
    global lang
    global frames
    global convert_cat_dico
    global convert_model_dico

    messages_bundle = messages_bundles
    cursor = cursors
    conn = conns
    lang = langs
    convert_cat_dico = {messages_bundle['cadre']: 0, messages_bundle['int_prof']: 1, messages_bundle['employe']: 2, messages_bundle['ouvrier']: 3, messages_bundle['chef_den']: 4, messages_bundle['agri']: 5}
    convert_model_dico = {messages_bundle['model']: 0,
                          messages_bundle['weighted_model']: 1,
                          messages_bundle['resampled_model']: 2,
                          messages_bundle['random_forest_precision']: 3,
                          messages_bundle['random_forest_recall']: 4}

    frames['window'] = window
    frames['current'] = affect_tab
    config.configure_style()
    dashboard_frame = tk.Frame(frames['current'], background="#f1f1f1")
    error_label = ttk.Label(dashboard_frame, text="", foreground='red')
    error_label.grid(row=0, columnspan=3)
    database = pd.read_csv("./data/generated_data.tsv", sep="\t")
    nb_rows = len(database.index)
    ttk.Label(dashboard_frame, text=messages_bundle['generate_data']+":").grid(row=1, column=0, sticky='e')
    check_dico['varGene'] = tk.IntVar()
    check_dico['check_gene'] = tk.Checkbutton(dashboard_frame, text="", variable=check_dico['varGene'], onvalue=1, offvalue=0, background="#f1f1f1")
    check_dico['check_gene'].grid(row=1, column=1, sticky='w')
    label_nbr = ttk.Label(dashboard_frame, text=messages_bundle['wanted_nbr']+":")
    label_nbr.grid(row=2, column=0, sticky='e')
    check_dico['varNbr'] = tk.StringVar()
    max_bound = 100000000
    gene_dico['nbr_spin'] = tk.Spinbox(dashboard_frame, from_=0, to=max_bound, increment=1, state='normal', textvariable=check_dico['varNbr'], highlightbackground="#f1f1f1", buttonbackground="#f1f1f1")
    gene_dico['nbr_spin'].grid(row=2, column=1)
    check_dico['varNbr'].trace('w', lambda *args: check_int(check_dico['varNbr'], gene_dico['nbr_spin'], error_label))
    if nb_rows == 0:
        error_label.configure(text=messages_bundle['must_data_2'])
        current_state['global'] = EPY_ERROR
        check_dico['check_gene'].select()
    else:
        check_dico['check_gene'].deselect()
    ttk.Label(dashboard_frame, text="").grid(row=3, column=0)
    ttk.Label(dashboard_frame, text=messages_bundle['compute_prob']+":").grid(row=4, column=0, sticky='e')
    check_dico['varEval'] = tk.IntVar()
    check_dico['check_eval'] = tk.Checkbutton(dashboard_frame, text="", variable=check_dico['varEval'], onvalue=1, offvalue=0, background="#f1f1f1")
    check_dico['check_eval'].grid(row=4, column=1, sticky='w')
    ttk.Label(dashboard_frame, text=messages_bundle['model_type']+":").grid(row=5, column=0, sticky='e')
    type_vals = [0, 1, 2, 3, 4]
    check_dico['varVals'] = tk.IntVar()
    check_dico['varVals'].set(0)
    for i in range(len(convert_model_dico)):
        eval_dico["type_{}".format(i)] = tk.Radiobutton(dashboard_frame, variable=check_dico['varVals'], text=list(convert_model_dico.keys())[i], value=type_vals[i], background="#f1f1f1")
        eval_dico["type_{}".format(i)].grid(row=i+5, column=1, sticky='w')
    eval_dico['perf_but'] = ttk.Button(dashboard_frame, text=messages_bundle['performance'], state='normal', command=performances)
    eval_dico['perf_but'].grid(row=5, rowspan=len(convert_model_dico), column=2, padx=10)
    ttk.Label(dashboard_frame, text=messages_bundle['csp']+":").grid(row=1, column=3, sticky='e')
    for cat in convert_cat_dico:
        cat_num = convert_cat_dico[cat]
        ttk.Label(dashboard_frame, text="{} : {}".format(cat_num, cat)).grid(row=cat_num+1, column=4, sticky='w')
    ttk.Label(dashboard_frame, text=messages_bundle['threshold']+":").grid(row=len(convert_model_dico)+5, column=0, sticky='e')
    check_dico['varThreshold'] = tk.DoubleVar()
    check_dico['varThreshold'].set(50)
    eval_dico['threshold_scale'] = tk.Scale(dashboard_frame, from_=0, to=100, orient='horizontal', variable=check_dico['varThreshold'], background='#f1f1f1', tickinterval=20, length=150)
    eval_dico['threshold_scale'].grid(row=len(convert_model_dico)+5, column=1, sticky='w')
    dashboard_frame.pack()
    button_frame = tk.Frame(frames['current'], background="#f1f1f1")
    button_dico['reset_button'] = ttk.Button(button_frame, text=messages_bundle['reset_button'], state='disabled', command=reset)
    button_dico['reset_button'].grid(row=0, column=0, padx=10, pady=10)
    button_dico['apply_button'] = ttk.Button(button_frame, text=messages_bundle['apply_button'], state='disabled', command=lambda: apply(frames['current'], frames['window'], error_label))
    button_dico['apply_button'].grid(row=0, column=1, padx=10, pady=10)
    button_frame.pack()
    check_dico['varGene'].trace('w', lambda *args: trace_function(check_dico['varGene'], check_dico['varEval'], gene_dico, True))
    check_dico['varEval'].trace('w', lambda *args: trace_function(check_dico['varEval'], check_dico['varGene'], eval_dico, False))
    check_dico['check_eval'].deselect()
    check_dico['check_gene'].deselect()
    frames['plot'] = None
    if not current_state['global'] == EPY_ERROR:
        is_result = len(list(database.columns)[-1]) > 6
        frames['display'] = display_data(frames['current'], database, is_result)
    else:
        frames['display'] = None
        check_dico['check_eval'].configure(state="disabled")
