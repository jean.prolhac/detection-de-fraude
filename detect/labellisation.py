import numpy as np
import pandas as pd


parameters_df = pd.read_csv("./detect/parameters/distribution_parameters.csv")
cat_list = list(parameters_df['SPC'].to_numpy())
database = pd.read_csv("./detect/train/generated_data.tsv", sep="\t")
data_cat = list(database["cat"])
data_sal = list(database["wage"])
data_dep = list(database["expenses"])
data_pat = list(database["estate"])
data_catm = [[], [], [], [], [], []]

for i in range(len(data_cat)):
    elem = data_cat[i]
    if elem == 0:
        data_catm[0].append(i)
    elif elem == 1:
        data_catm[1].append(i)
    elif elem == 2:
        data_catm[2].append(i)
    elif elem == 3:
        data_catm[3].append(i)
    elif elem == 4:
        data_catm[4].append(i)
    else:
        data_catm[5].append(i)


def is_fraudster_1():
    V = []
    for cat in range(len(cat_list)):
        X = [data_sal[i] for i in data_catm[cat]]
        Y = [data_pat[i] for i in data_catm[cat]]
        Z = [data_dep[i] for i in data_catm[cat]]
        moy_Y = sum(Y)/len(Y)
        for i in range(len(X)):
            if Y[i] >= (max(Y)-((max(Y)-moy_Y)/10)-moy_Y)/(max(X)-min(X))*(X[i]-min(X))+ moy_Y:
                if Z[i] >= 1.4*X[i]:
                    V.append(1)
                else:
                    V.append(0)
            else:
                V.append(0)
    print(sum(V))
    return V


def is_fraudster_2():
    V = []
    for cat in range(len(cat_list)):
        X = [data_sal[i] for i in data_catm[cat]]
        Y = [data_pat[i] for i in data_catm[cat]]
        Z = [data_dep[i] for i in data_catm[cat]]
        N = [Z[i]-X[i] for i in range(len(X))]
        moy_Y = sum(Y)/len(Y)
        moy_N = sum(N)/len(N)
        for i in range(len(X)):
            if N[i] > moy_N+0.35*(max(N)+moy_N) and N[i] > 0:
                if Y[i] < moy_Y-0.35*(moy_Y-min(Y)):
                    V.append(1)
                else:
                    V.append(0)
            else:
                V.append(0)
    print(sum(V))
    return V


def is_fraudster_3():
    V = []
    for cat in range(len(cat_list)):
        X = [data_sal[i] for i in data_catm[cat]]
        Y = [data_pat[i] for i in data_catm[cat]]
        Z = [data_dep[i] for i in data_catm[cat]]
        N = [Z[i]-X[i] for i in range(len(X))]
        quant = np.quantile(N, 0.8)
        quant2 = np.quantile(Y, 0.2)
        for i in range(len(X)):
            if N[i] > quant and N[i] > 0:
                if Y[i] < quant2:
                    V.append(1)
                else:
                    V.append(0)
            else:
                V.append(0)
    print(sum(V))
    return V


def is_fraudster_4():
    V = []
    for cat in range(len(cat_list)):
        X = [data_sal[i] for i in data_catm[cat]]
        Y = [data_pat[i] for i in data_catm[cat]]
        moy_X = sum(X)/len(X)
        moy_Y = sum(Y)/len(Y)
        a = (int(max(X))+1-int(min(X)))/1.5
        b = (max(Y)-moy_Y)*0.6
        for i in range(len(X)):
            if 1-((X[i]-(moy_X*1.2))**2)/(a**2) >= 0:
                if Y[i] > moy_Y+((1-((X[i]-(moy_X*1.2))**2)/(a**2))*b**2)**0.5:
                    V.append(1)
                else:
                    V.append(0)
            else:
                if Y[i] > moy_Y:
                    V.append(1)
                else:
                    V.append(0)
    print(sum(V))
    return V


def labellisation():
    ind_1 = is_fraudster_1()
    ind_2 = is_fraudster_2()
    ind_3 = is_fraudster_3()
    ind_4 = is_fraudster_4()
    somme = []
    for k in range(len(ind_1)):
        if ind_1[k]+ind_2[k]+ind_3[k]+ind_4[k] > 0:
            somme.append(1)
        else:
            somme.append(0)
    database["is_fraudster_1"] = ind_1
    database["is_fraudster_2"] = ind_2
    database["is_fraudster_3"] = ind_3
    database["is_fraudster_4"] = ind_4
    database["is_fraudster"] = somme
    database.to_csv('./detect/train/generated_data_labelled.tsv', sep='\t')


labellisation()
