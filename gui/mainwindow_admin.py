"""The main window of the FraudDetect GUI.
"""

import tkinter as tk
import csv

from tkinter import ttk
import gui.gui_config as config
from rh.frame import add_widgets as rh_add_widgets
from members.new_member import add_new_member as mem_add_widgets, button_frm_widgets
from affectations.frame import add_widgets as affect_add_widgets
from detect.frame import add_widgets as detect_add_widgets
from inference.frame import add_widgets as inference_add_widgets
from PIL import Image, ImageTk

# The messages bundle
messages_bundle = {}

# The object used to query the database.
cursor = None

# The object used to connect to the database.
conn = None

# The language of the application.
lang = None

# When the user clicks one of the buttons on the left menu, 
# a tab is opened on the right side of the window.
# There are three tabs: one for managing the student data, another to
# add a new registration and another to edit the registrations.
tabs = {"Teams composition" : False, "Members": False, "Affectations": False, "Data": False, "Inference": False}
open_window = {"current": None, "previous": None}
buttons = {}
windows = {}
translation = {}

# The ttk.Notebook (frame) containing the above tabs.
nb = None

def destroy_tab(event, tab_name, button):
    """Destroys a given tab.

    Parameters
    ----------
    event : 
        Information on the event.
    tab_name : string
        The name of the tab to destroy.
    button : ttk.Button
        The button on the left side used to open the tab.
    """
    global nb
    config.reset_active_button()
    button.state(["!disabled"])
    tabs[tab_name] = None
    if not is_tab_open():
        nb.destroy()
        nb = None


def select_tab(event, button):
    """Invoked when a tab is selected.

    Parameters
    ----------
    event : 
        The information about the selection event.
    button : ttk.Button
        The button used to open the selected tab.
    """
    # When a tab is selected, the button used to open it becomes the active one and 
    # changes its color.
    config.reset_active_button()
    config.set_active_button(button)


def is_tab_open():
    """Checks whether any tab is open.

    Returns
    -------
    bool
        True if any of the three tabs is open, False otherwise.
    """
    for value in list(tabs.values()):
        if value is not None:
            return True
    return False


def open_add_edit_team_tab_admin(window, current_frame, buttons):
    """Opens the tab used to manage the student data.

    Parameters
    ----------
    window : tk.Tk
        The FraudDetect main window.
    btn_add_edit_student : ttk.Button
        The button used to open the tab.

    """
    btn_add_edit_student = buttons["add_edit_student"]
    btn_previous = buttons["previous"]
    if tabs["Teams composition"] is True :
        config.reset_active_button()
        if not open_window["current"] is None:
            open_window["current"][1].destroy()
        if not open_window["previous"] is None:
            open_window["previous"][1].destroy()
        open_window["previous"] = open_window["current"]
        open_window["current"] = None
        for elem in list(tabs.keys()):
            tabs[elem] = False
        btn_previous.configure(state="disabled")
    else:
        # We set the button used to open the tab as active
        config.reset_active_button()
        config.set_active_button(btn_add_edit_student)
        btn_previous.configure(state="active")
        # Create the frame that contains the tab.
        if open_window["current"] is not None:
            open_window["current"][1].destroy()
            for elem in list(tabs.keys()):
                if elem!="Teams composition":
                    tabs[elem] = False
        stud_tab = ttk.Frame(current_frame, style="Tab.TFrame")
        rh_add_widgets(window, stud_tab, messages_bundle, cursor, conn, lang)
        stud_tab.grid(row=0, column=0, ipadx=10, sticky="nsew")

        open_window["previous"] = open_window["current"]
        open_window["current"] = ("Teams composition", stud_tab)
        tabs["Teams composition"] = True

    # Updates the window to force the tab to display.
    window.update()


def open_add_edit_member_tab_admin(window, buttons):
    """Opens the tab used to manage the student data.

    Parameters
    ----------
    window : tk.Tk
        The FraudDetect main window.
    btn_add_edit_student : ttk.Button
        The button used to open the tab.

    """
    btn_add_edit_member = buttons["add_edit_member"]
    btn_previous = buttons["previous"]
    if tabs["Members"] is True :
        config.reset_active_button()
        if not open_window["current"] is None:
            open_window["current"][1].destroy()
        if not open_window["previous"] is None:
            open_window["previous"][1].destroy()
        open_window["previous"] = open_window["current"]
        open_window["current"] = None
        for elem in list(tabs.keys()):
            tabs[elem] = False
        btn_previous.configure(state="disabled")
    else:
        # We set the button used to open the tab as active
        config.reset_active_button()
        config.set_active_button(btn_add_edit_member)
        btn_previous.configure(state="active")
        # Create the frame that contains the tab.
        if open_window["current"] is not None:
            open_window["current"][1].destroy()
            for elem in list(tabs.keys()):
                if elem!="Members":
                    tabs[elem] = False
        mem_tab = ttk.Frame(window, style="Tab.TFrame")
        mem_add_widgets(messages_bundle, cursor, conn, mem_tab, None)
        mem_tab.grid(row=0, column=0, ipadx=10, sticky="nsew")

        open_window["previous"] = open_window["current"]
        open_window["current"] = ("Members", mem_tab)
        tabs["Members"] = True

    # Updates the window to force the tab to display.
    window.update()


def open_affectations(window, frm_intro, buttons):
    """Opens the tab used to manage the student data.

    Parameters
    ----------
    window : tk.Tk
        The FraudDetect main window.
    btn_add_edit_student : ttk.Button
        The button used to open the tab.

    """
    btn_affectation = buttons["affectations"]
    btn_previous = buttons["previous"]
    if tabs["Affectations"] is True :
        config.reset_active_button()
        if not open_window["current"] is None:
            open_window["current"][1].destroy()
        if not open_window["previous"] is None:
            open_window["previous"][1].destroy()
        open_window["previous"] = open_window["current"]
        open_window["current"] = None
        for elem in list(tabs.keys()):
            tabs[elem] = False
        btn_previous.configure(state="disabled")
    else:
        # We set the button used to open the tab as active
        config.reset_active_button()
        config.set_active_button(btn_affectation)
        btn_previous.configure(state="active")
        # Create the frame that contains the tab.
        if open_window["current"] is not None:
            open_window["current"][1].destroy()
            for elem in list(tabs.keys()):
                if elem!="Affectations":
                    tabs[elem] = False
        mem_tab = ttk.Frame(frm_intro, style="Tab.TFrame")
        affect_add_widgets(window, mem_tab, messages_bundle, cursor, conn, lang)
        mem_tab.grid(row=0, column=0, ipadx=10, sticky="nsew")

        open_window["previous"] = open_window["current"]
        open_window["current"] = ("Affectations", mem_tab)
        tabs["Affectations"] = True

    # Updates the window to force the tab to display.
    window.update()


def open_data(window, frm_intro, buttons):
    """Opens the tab used to manage the student data.

    Parameters
    ----------
    window : tk.Tk
        The FraudDetect main window.
    btn_add_edit_student : ttk.Button
        The button used to open the tab.

    """
    btn_affectation = buttons["data"]
    btn_previous = buttons["previous"]
    if tabs["Data"] is True :
        config.reset_active_button()
        if not open_window["current"] is None:
            open_window["current"][1].destroy()
        if not open_window["previous"] is None:
            open_window["previous"][1].destroy()
        open_window["previous"] = open_window["current"]
        open_window["current"] = None
        for elem in list(tabs.keys()):
            tabs[elem] = False
        btn_previous.configure(state="disabled")
    else:
        # We set the button used to open the tab as active
        config.reset_active_button()
        config.set_active_button(btn_affectation)
        btn_previous.configure(state="active")
        # Create the frame that contains the tab.
        if open_window["current"] is not None:
            open_window["current"][1].destroy()
            for elem in list(tabs.keys()):
                if elem!="Data":
                    tabs[elem] = False
        mem_tab = ttk.Frame(frm_intro, style="Tab.TFrame")
        detect_add_widgets(window, mem_tab, messages_bundle, cursor, conn, lang)
        mem_tab.grid(row=0, column=0, ipadx=10, sticky="nsew")

        open_window["previous"] = open_window["current"]
        open_window["current"] = ("Data", mem_tab)
        tabs["Data"] = True

    # Updates the window to force the tab to display.
    window.update()


def open_inference(window, frm_intro, buttons):
    """Opens the tab used to manage the student data.

    Parameters
    ----------
    window : tk.Tk
        The FraudDetect main window.
    btn_add_edit_student : ttk.Button
        The button used to open the tab.

    """
    btn_infer = buttons["inference"]
    btn_previous = buttons["previous"]
    if tabs["Inference"] is True :
        config.reset_active_button()
        if not open_window["current"] is None:
            open_window["current"][1].destroy()
        if not open_window["previous"] is None:
            open_window["previous"][1].destroy()
        open_window["previous"] = open_window["current"]
        open_window["current"] = None
        for elem in list(tabs.keys()):
            tabs[elem] = False
        btn_previous.configure(state="disabled")
    else:
        # We set the button used to open the tab as active
        config.reset_active_button()
        config.set_active_button(btn_infer)
        btn_previous.configure(state="active")
        # Create the frame that contains the tab.
        if open_window["current"] is not None:
            open_window["current"][1].destroy()
            for elem in list(tabs.keys()):
                if elem!="Inference":
                    tabs[elem] = False
        mem_tab = ttk.Frame(frm_intro, style="Tab.TFrame")
        inference_add_widgets(window, mem_tab, messages_bundle, cursor, conn, lang)
        mem_tab.grid(row=0, column=0, ipadx=10, sticky="nsew")

        open_window["previous"] = open_window["current"]
        open_window["current"] = ("Inference", mem_tab)
        tabs["Inference"] = True

    # Updates the window to force the tab to display.
    window.update()


def open_main_window_admin(_cursor, _conn, _messages_bundle, _lang):
    """Opens the FraudDetect main window.

    Parameters
    ----------
    _cursor : 
        The object used to query the database.
    _conn : 
        The object used to connect to the database.
    _messages_bundle : dictionary
        The messages bundle
    _lang : string
        The language of the application.
    """
    global messages_bundle
    global cursor
    global conn
    global lang
    
    # Initializes the global variables.
    messages_bundle = _messages_bundle
    cursor = _cursor
    conn = _conn
    lang = _lang

    # Open a new window
    window = tk.Tk()
    window.title("FraudDetect")

    # Add an icon to the window.
    icon = config.load_icon_image()
    window.iconphoto(False, icon)

    # Set the window as non-resizable.
    window.resizable(False, False)

    # The main window consists of a 1x2 grid. 
    # We configure the row so as the widgets span the height of the window.
    # THe second column (the one that hosts the tabs) spans the whole space after the column 0
    # (that contains the left menu).
    window.rowconfigure(0, weight=1)
    window.columnconfigure(1, weight=1)

    # Configure the style of the window.
    config.configure_style()

    # The frame that contains the logo FD.
    frm_intro = ttk.Frame(window)

    # Add this frame to the second column (next to the left menu).
    frm_intro.grid(row=0, column=1, sticky='nsew')

    # Add the background image.
    image = Image.open("./gui/icons/icon.jpg")
    image = image.resize((400, 400))
    #image.putalpha(128)
    image = ImageTk.PhotoImage(image)
    ttk.Label(frm_intro, borderwidth=0, image=image).grid(row=0, column=0)
    windows["default"] = frm_intro
    
    # Add the left menu with the three buttons.
    frm_menu = ttk.Frame(window, style="Menu.TFrame")
    btn_previous = ttk.Button(frm_menu, text=u"\u2190", style="Menu.TButton", command=lambda: previous_btn(buttons))
    btn_add_edit_stud = ttk.Button(frm_menu, text=messages_bundle["add_edit_member"], style="Menu.TButton", \
        command=lambda: open_add_edit_team_tab_admin(window, frm_intro, buttons))
    translation["Teams composition"] = lambda: open_add_edit_team_tab_admin(window, frm_intro, buttons)
    btn_add_edit_stud.grid(row=1, column=0, padx=5, pady=5, ipadx=20, ipady=5, sticky='ew')
    
    btn_add_edit_mem = ttk.Button(frm_menu, text=messages_bundle["update_add_member"], style="Menu.TButton", \
        command=lambda: open_add_edit_member_tab_admin(frm_intro, buttons))
    btn_add_edit_mem.grid(row=2, column=0, padx=5, pady=5, ipadx=20, ipady=5, sticky='ew')
    translation["Members"] = lambda: open_add_edit_member_tab_admin(frm_intro,buttons)

    btn_affectation = ttk.Button(frm_menu, text=messages_bundle["affectation"], style="Menu.TButton", \
        command=lambda: open_affectations(window, frm_intro, buttons))
    btn_affectation.grid(row=3, column=0, padx=5, pady=5, ipadx=20, ipady=5, sticky='ew')
    translation["Affectations"] = lambda: open_affectations(window, frm_intro, buttons)

    btn_data = ttk.Button(frm_menu, text=messages_bundle["data"], style="Menu.TButton", \
        command=lambda: open_data(window, frm_intro, buttons))
    btn_data.grid(row=4, column=0, padx=5, pady=5, ipadx=20, ipady=5, sticky='ew')
    translation["Data"] = lambda: open_data(window, frm_intro, buttons)

    btn_infer = ttk.Button(frm_menu, text=messages_bundle["inference"], style="Menu.TButton", \
        command=lambda: open_inference(window, frm_intro, buttons))
    btn_infer.grid(row=5, column=0, padx=5, pady=5, ipadx=20, ipady=5, sticky='ew')
    translation["Inference"] = lambda: open_inference(window, frm_intro, buttons)

    btn_previous.grid(row=0, column=0, padx=5, pady=0, ipadx=20, ipady=5, sticky='ew')
    frm_menu.grid(row=0, column=0, sticky='nsew')
    frm_menu.columnconfigure(0, weight=1)
    btn_previous.configure(state="disabled")
    buttons["add_edit_student"] = btn_add_edit_stud
    buttons["add_edit_member"] = btn_add_edit_mem
    buttons["affectations"] = btn_affectation
    buttons["previous"] = btn_previous
    buttons["data"] = btn_data
    buttons["inference"] = btn_infer

    # Start the event loop
    window.mainloop()


def previous_btn(buttons):
    btn_previous = buttons["previous"]
    if open_window["previous"] is None:
        open_window["current"][1].destroy()
        for elem in list(tabs.keys()):
            tabs[elem] = False
        open_window["previous"] = open_window["current"]
        open_window["current"] = None
        btn_previous.configure(state="disabled")
        config.reset_active_button()
    else:
        open_window["current"][1].destroy()
        for elem in list(tabs.keys()):
            if elem!=open_window["previous"][0]:
                tabs[elem] = False
        translation[open_window["previous"][0]]()
        

def welcome_page(window):
    frm_intro = ttk.Frame(window)
    # Add this frame to the second column (next to the left menu).
    frm_intro.grid(row=0, column=1, sticky='nsew')
    # Add the background image.
    image = Image.open("./gui/icons/icon.jpg")
    image = image.resize((400, 400))
    image.putalpha(128)
    image = ImageTk.PhotoImage(image)
    ttk.Label(frm_intro, borderwidth=0, image=image).grid(row=0, column=0)
    windows["frm_intro"] = frm_intro


def members_page(window):
    frm_intro = ttk.Frame(window)
    # Add this frame to the second column (next to the left menu).
    frm_intro.grid(row=0, column=1, sticky='nsew')
    stud_tab = ttk.Frame(frm_intro, style="Tab.TFrame")
    rh_add_widgets(stud_tab, messages_bundle, cursor, conn, lang)
    stud_tab.grid(row=0, column=0, ipadx=10, sticky="nsew")
    windows["frm_intro"] = frm_intro
 