import pandas as pd
import numpy as np
import tkinter as tk
from tkinter import ttk
from tensorflow import keras
import pickle
import joblib
import sys
sys.path.append('.')
import gui.gui_config as config


global model_dico
global scaler

model_dico = {'model': [True], 'weighted_model': [True], 'resampled_model': [True], 'random_forest_precision': [False], 'random_forest_recall': [False]}
scaler = pickle.load(open('./detect/parameters/scaler.pkl', 'rb'))
for model_type in model_dico:
    if model_dico[model_type][0]:
        model_dico[model_type].append(keras.models.load_model('./detect/parameters/{}'.format(model_type)))
    else:
        model_dico[model_type].append(joblib.load('./detect/parameters/{}.pkl'.format(model_type)))


def inference(type_model, cat, wage, expenses, estate):
    global model_dico
    global scaler
    if model_dico[type_model][0]:
        to_predict = scaler.transform(np.array([[cat, wage, expenses, estate]]))
        return round(float(model_dico[type_model][1].predict(to_predict)[0][0]), 3)
    else:
        to_predict = pd.DataFrame(np.array([[cat, wage, expenses, estate]]), columns=['cat', 'wage', 'Log expenses', 'Log estate'])
        return round(float(model_dico[type_model][1].predict_proba(to_predict)[0,1]), 3)



global messages_bundle
global cursor
global conn
global lang
global convert_cat_dico
global convert_model_dico
global state_dico
global error_dico
global button_dico
global frames
global widgets

INIT_STATE = 0
ERROR_STATE = 1
state_dico = {'global': ERROR_STATE, 'sal': ERROR_STATE, 'dep': ERROR_STATE, 'pat': ERROR_STATE}
error_dico = {}
button_dico = {}
frames = {}
widgets = {}
convert_cat_dico = {}
convert_model_dico = {}
messages_bundle = {}
cursor = None
conn = None
lang = None


def convert_numbers(number, long_nbr, lang):
    if lang=="fr":
        if long_nbr:
            return "{:,.2f}".format(number).replace(',', ' ').replace('.', ',')
        else:
            return "{}".format(number).replace('.', ',')
    else:
        if long_nbr:
            return "{:,.2f}".format(number)
        else:
            return str(number)


def update_state(lbl):
    global state_dico
    global error_dico
    global button_dico
    state_dico['global'] = max(list(state_dico.values())[1:])
    lbl.configure(text=error_dico[state_dico['global']])
    if state_dico['global'] == INIT_STATE:
        button_dico['apply_button'].configure(state='normal')
    else:
        button_dico['apply_button'].configure(state='disabled')


def trace_function(name, lbl):
    global state_dico
    global button_dico
    global widgets
    button_dico['clear_button'].configure(state='normal')
    to_try = widgets['var'+name.capitalize()].get()
    try:
        float(to_try)
        state_dico[name] = INIT_STATE
        widgets['enter_'+name].configure(highlightbackground="#f1f1f1")
    except ValueError:
        state_dico[name] = ERROR_STATE
        widgets['enter_'+name].configure(highlightbackground="red")
    update_state(lbl)


def clear():
    global button_dico
    global widgets
    widgets['varMod'].set(messages_bundle['model'])
    widgets['varCat'].set(messages_bundle['cadre'])
    widgets['varSal'].set('')
    widgets['varDep'].set('')
    widgets['varPat'].set('')
    button_dico['clear_button'].configure(state='disabled')


def apply(convert_mod_dico, convert_cat_dico):
    global button_dico
    global frames
    global widgets
    button_dico['apply_button'].configure(state='disabled')
    button_dico['cancel_button'].configure(state='normal')
    if frames['result_frame'] is not None:
        frames['result_frame'].destroy()
    frames['result_frame'] = tk.Frame(frames['current'], background="#f1f1f1")
    ttk.Label(frames['result_frame'], text=messages_bundle['probability']+":").grid(row=0, column=0)
    res = inference(convert_mod_dico[widgets['varMod'].get()], convert_cat_dico[widgets['varCat'].get()], float(widgets['varSal'].get()), float(widgets['varDep'].get()), float(widgets['varPat'].get()))
    ttk.Label(frames['result_frame'], text="{}".format(convert_numbers(res, False, lang))).grid(row=0, column=1)
    frames['result_frame'].pack()
    frames['window'].update()


def cancel():
    global frames
    global button_dico
    button_dico['cancel_button'].configure(state='disabled')
    frames['result_frame'].destroy()


def add_widgets(window, affect_tab, messages_bundles, cursors, conns, langs):
    global messages_bundle
    global cursor
    global conn
    global lang
    global frames
    global convert_cat_dico
    global convert_model_dico
    global widgets
    global error_dico

    messages_bundle = messages_bundles
    cursor = cursors
    conn = conns
    lang = langs
    convert_cat_dico = {messages_bundle['cadre']: 0, messages_bundle['int_prof']: 1, messages_bundle['employe']: 2, messages_bundle['ouvrier']: 3, messages_bundle['chef_den']: 4, messages_bundle['agri']: 5}
    error_dico = {0: "", 1: messages_bundle['positive_number']}

    frames['window'] = window
    frames['current'] = affect_tab
    config.configure_style()

    main_frame = tk.Frame(frames['current'], background="#f1f1f1")
    ttk.Label(main_frame, text=messages_bundle['model_type']+":").grid(row=0, column=0, sticky='e', pady=5)
    convert_mod_dico = {messages_bundle['model']: "model",
                        messages_bundle['weighted_model']: "weighted_model",
                        messages_bundle['resampled_model']: "resampled_model",
                        messages_bundle['random_forest_precision']: "random_forest_precision",
                        messages_bundle['random_forest_recall']: "random_forest_recall"}
    mod_list = list(convert_mod_dico.keys())
    widgets['varMod'] = tk.StringVar()
    widgets['varMod'].set(messages_bundle['model'])
    widgets['choose_mod'] = tk.OptionMenu(main_frame, widgets['varMod'], *mod_list)
    widgets['choose_mod'].config(width=10)
    widgets['choose_mod'].grid(row=0, column=1, sticky='we')
    ttk.Label(main_frame, text=messages_bundle['csp']+":").grid(row=1, column=0, sticky='e', pady=5)
    cat_list = list(convert_cat_dico.keys())
    widgets['varCat'] = tk.StringVar()
    widgets['varCat'].set(messages_bundle['cadre'])
    widgets['choose_cat'] = tk.OptionMenu(main_frame, widgets['varCat'], *cat_list)
    widgets['choose_cat'].config(width=10)
    widgets['choose_cat'].grid(row=1, column=1, sticky='we')
    ttk.Label(main_frame, text=messages_bundle['wage']+":").grid(row=2, column=0, sticky='e', pady=5)
    widgets['varSal'] = tk.StringVar()
    widgets['varSal'].set('')
    widgets['enter_sal'] = tk.Entry(main_frame, justify='center', textvariable=widgets['varSal'], highlightbackground="#f1f1f1")
    widgets['enter_sal'].grid(row=2, column=1)
    ttk.Label(main_frame, text=messages_bundle['expenses']+":").grid(row=3, column=0, sticky='e', pady=5)
    widgets['varDep'] = tk.StringVar()
    widgets['varDep'].set('')
    widgets['enter_dep'] = tk.Entry(main_frame, justify='center', textvariable=widgets['varDep'], highlightbackground="#f1f1f1")
    widgets['enter_dep'].grid(row=3, column=1)
    ttk.Label(main_frame, text=messages_bundle['estate']+":").grid(row=4, column=0, sticky='e', pady=5)
    widgets['varPat'] = tk.StringVar()
    widgets['varPat'].set('')
    widgets['enter_pat'] = tk.Entry(main_frame, justify='center', textvariable=widgets['varPat'], highlightbackground="#f1f1f1")
    widgets['enter_pat'].grid(row=4, column=1)
    error_lbl = ttk.Label(main_frame, text=messages_bundle['positive_number'], foreground="red")
    error_lbl.grid(row=5, column=0, columnspan=2, pady=5)
    widgets['varSal'].trace('w', lambda *args: trace_function('sal', error_lbl))
    widgets['varDep'].trace('w', lambda *args: trace_function('dep', error_lbl))
    widgets['varPat'].trace('w', lambda *args: trace_function('pat', error_lbl))
    main_frame.pack()
    button_frame = tk.Frame(frames['current'], background="#f1f1f1")
    button_dico['cancel_button'] = ttk.Button(button_frame, text=messages_bundle['cancel_button'], state='disabled', command=cancel)
    button_dico['cancel_button'].grid(row=0, column=0, padx=10, pady=10)
    button_dico['clear_button'] = ttk.Button(button_frame, text=messages_bundle['clear_button'], state='disabled', command=clear)
    button_dico['clear_button'].grid(row=0, column=1, padx=10, pady=10)
    button_dico['apply_button'] = ttk.Button(button_frame, text=messages_bundle['apply_button'], state='disabled', command=lambda: apply(convert_mod_dico, convert_cat_dico))
    button_dico['apply_button'].grid(row=0, column=2, padx=10, pady=10)
    button_frame.pack()
    frames['result_frame'] = None
