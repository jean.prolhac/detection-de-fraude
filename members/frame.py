import tkinter as tk
from tkinter import ttk
import sqlite3
import sys, os
sys.path.append('.')
import gui.gui_config as config

widgets_stock = {}

INIT_STATE = 0
WRONG_MODIF = 1
GOOD_MODIF = 2

UNEXPECTED_ERROR = -1

convertion = {4: "email_state", 5: "phone_number_state", 2:"first_name_state", 3:"last_name_state", "email_state":4, "phone_number_state":5, "first_name_state":2, "last_name_state":3}

def add_widgets(modif_tab, _messages_bundle, data, team_name, curs, connex):
    global messages_bundle
    global cursor
    global conn

    state_dict = {"first_name_state" : [], "last_name_state" : [], "email_state" : [], "phone_number_state" : []}
    widgets_stock_frm = {"registration_data_var" : [], "registration_data" : [], "current_student_registrations" : [], "state_dict" : state_dict, "buttons" : {}, "modif" : modif_tab, "lines" : {}}
    widgets_stock[team_name] = widgets_stock_frm

    # We assign the passed arguments to the global variables.
    messages_bundle = _messages_bundle
    cursor = curs
    conn = connex
    config.configure_style()

    registration_frm = tk.Frame(widgets_stock[team_name]["modif"], background="#dcdad5")
    registration_frm.pack(fill="both", expand=True)
    canvas = tk.Canvas(registration_frm, highlightthickness=0, bd=0, background="#dcdad5")
    canvas.pack(side="left", fill="both", expand=True)

    scrollbar = ttk.Scrollbar(registration_frm, orient="vertical", command=canvas.yview)
    scrollbar.pack(side="right", fill="y")
    scroll_registration_frm = ttk.Frame(canvas, style="Table.TFrame")

    canvas_frm = canvas.create_window((0, 0), window=scroll_registration_frm, anchor="ne")
    canvas.configure(yscrollcommand=scrollbar.set)

    def config_canvas(e):
        canvas.configure(scrollregion=canvas.bbox("all"))
        canvas.yview_moveto(0)
        canvas.itemconfig(canvas_frm, width=e.width)

    canvas.bind(
        "<Configure>", 
        lambda e: config_canvas(e)
    )

    scroll_registration_frm.columnconfigure(0, weight=1)
    for i in range(1,6):
        scroll_registration_frm.columnconfigure(i, weight=3)

    scroll_registration_frm.configure(style="Table.TFrame")

    widgets_stock[team_name]["registration_data_var"].append((tk.IntVar(),))

    button_frm = tk.Frame(widgets_stock[team_name]["modif"],background="#dcdad5")
    _buttons_frm_widgets(button_frm, team_name)
    button_frm.pack()

    _add_members(scroll_registration_frm, team_name, data)

def lemailestbon(mail):
    liste = list(mail)
    if '.' not in liste :
        return False
    card_at = 0
    for e in liste :
        if e =='@':
            card_at += 1
    if card_at != 1 :
        return False

    indice_at = 0
    dernier_indice_pt = 0
    if liste[-1] == '.' :
        return False

    for i in range(len(liste)-1):
        if liste[i] == '@':
            indice_at = i
        if liste[i] == '.':
            if dernier_indice_pt == i-1 or liste[i+1] == '@':
                return False
            dernier_indice_pt = i

    if dernier_indice_pt < indice_at:
        return False

    return True


def trace_phone(line_name, team_name, *args):
    index = widgets_stock[team_name]["lines"][line_name]
    phone_number = widgets_stock[team_name]["registration_data_var"][int(index)][5].get().strip()
    l = list(phone_number)
    vrai = True
    if len(l)!=10 : 
        vrai = False
    for elem in l:
        if elem not in [str(k) for k in range(10)]:
            vrai = False
    if vrai :
        widgets_stock[team_name]["state_dict"]["phone_number_state"][int(index)-1] = GOOD_MODIF
        widgets_stock[team_name]["registration_data"][int(index)][5].configure(highlightbackground="#dcdad5")
        if update_active(team_name):
            widgets_stock[team_name]["buttons"]["update"].configure(state="active")
    else:
        widgets_stock[team_name]["state_dict"]["phone_number_state"][int(index)-1] = WRONG_MODIF
        widgets_stock[team_name]["buttons"]["update"].configure(state="disabled")
        widgets_stock[team_name]["registration_data"][int(index)][5].configure(highlightbackground="red")
        widgets_stock[team_name]["buttons"]["label"].configure(text=messages_bundle["enter_phone_number"])
    widgets_stock[team_name]["buttons"]["clear"].configure(state="active")


def trace_email(line_name, team_name, *args):
    index = widgets_stock[team_name]["lines"][line_name]
    email = widgets_stock[team_name]["registration_data_var"][int(index)][4].get().strip()
    if lemailestbon(email) :
        widgets_stock[team_name]["state_dict"]["email_state"][int(index)-1] = GOOD_MODIF
        widgets_stock[team_name]["registration_data"][int(index)][4].configure(highlightbackground="#dcdad5")
        if update_active(team_name):
            widgets_stock[team_name]["buttons"]["update"].configure(state="active")
    else:
        widgets_stock[team_name]["state_dict"]["email_state"][int(index)-1] = WRONG_MODIF
        widgets_stock[team_name]["buttons"]["update"].configure(state="disabled")
        widgets_stock[team_name]["registration_data"][int(index)][4].configure(highlightbackground="red")
        widgets_stock[team_name]["buttons"]["label"].configure(text=messages_bundle["enter_email_address"])
    widgets_stock[team_name]["buttons"]["clear"].configure(state="active")


def update_active(team_name):
    if is_init(team_name):
        widgets_stock[team_name]["buttons"]["label"].configure(text="")
        return False
    else:
        if is_wrong(team_name):
            widgets_stock[team_name]["buttons"]["label"].configure(text=messages_bundle["correct_error"])
            return False
        else:
            widgets_stock[team_name]["buttons"]["label"].configure(text=messages_bundle["update"])
            return True


def is_init(team_name):
    for key in list(widgets_stock[team_name]["state_dict"].keys()):
        for elem in widgets_stock[team_name]["state_dict"][key]:
            if elem!=INIT_STATE:
                return False
    return True


def is_wrong(team_name):
    for key in list(widgets_stock[team_name]["state_dict"].keys()):
        for elem in widgets_stock[team_name]["state_dict"][key]:
            if elem==WRONG_MODIF:
                return True
    return False


def is_button_checked(index, team_name):
    """Returns whether the check button at the specified index (row) is checked or not.

    Returns
    -------
    bool
        True if the specified check button is checked, False otherwise.
    """
    return widgets_stock[team_name]["registration_data_var"][index][0].get()


def check_button(index, team_name):
    """Sets the check button at the specified index (row) as checked.
    """
    widgets_stock[team_name]["registration_data_var"][index][0].set(1)


def check_all_buttons(team_name):
    """Sets all the check buttons as checked.
    """
    for i in range(len(widgets_stock[team_name]["registration_data_var"])):
        if not is_button_checked(i, team_name):
            check_button(i, team_name)


def uncheck_button(index, team_name):
    """Sets the check button at the specified index as unchecked.
    """
    widgets_stock[team_name]["registration_data_var"][index][0].set(0)


def uncheck_all_buttons(team_name):
    """Sets all the check buttons as unchecked.
    """
    for i in range(len(widgets_stock[team_name]["registration_data_var"])):
        if is_button_checked(i, team_name):
            uncheck_button(i, team_name)


def check_delete(index, team_name):
    if is_button_checked(index, team_name):
        widgets_stock[team_name]["buttons"]["delete"].configure(state="active")
        widgets_stock[team_name]["buttons"]["label"].configure(text=messages_bundle["delete"])
    else:
        if get_selected_buttons(team_name)==[]:
            widgets_stock[team_name]["buttons"]["delete"].configure(state="disabled")
            widgets_stock[team_name]["buttons"]["label"].configure(text="")


def check_all_selected(team_name):
    """Invoked when the user checks/unchecks the button on the first row (indicating that s/he wants to select/
    deselect all rows).
    """
    # If the button is checked, we select all rows
    if len(widgets_stock[team_name]["registration_data_var"])>0:
        if is_button_checked(0, team_name):
            check_all_buttons(team_name)
            widgets_stock[team_name]["buttons"]["delete"].configure(state="active")
        else: # otherwise, we deselect all rows.
            uncheck_all_buttons(team_name)
            widgets_stock[team_name]["buttons"]["delete"].configure(state="disabled")
    else:
        widgets_stock[team_name]["buttons"]["delete"].configure(state="disabled")


def get_selected_buttons(team_name):
    L = []
    for i in range(1, len(widgets_stock[team_name]["registration_data_var"])):
        if is_button_checked(i,team_name):
            L.append(i)
    return L


def modified_entry(type, line_name, team_name, *args):
    index = widgets_stock[team_name]["lines"][line_name]
    if type=="first_name_state":
        k=2
    else:
        k=3
    entry = widgets_stock[team_name]["registration_data_var"][int(index)][k].get().strip()
    if len(entry)>0:
        widgets_stock[team_name]["state_dict"][type][int(index)-1] = GOOD_MODIF
        widgets_stock[team_name]["registration_data"][int(index)][k].configure(highlightbackground="#dcdad5")
        if update_active(team_name):
            widgets_stock[team_name]["buttons"]["update"].configure(state="active")
    else:
        widgets_stock[team_name]["state_dict"][type][int(index)-1] = WRONG_MODIF
        widgets_stock[team_name]["buttons"]["update"].configure(state="disabled")
        widgets_stock[team_name]["buttons"]["label"].configure(text=messages_bundle["correct_error"])
        widgets_stock[team_name]["registration_data"][int(index)][k].configure(highlightbackground="red")
    widgets_stock[team_name]["buttons"]["clear"].configure(state="active")


def delete_function(team_name):
    cursor.execute("BEGIN")
    for i in range(1,len(widgets_stock[team_name]["registration_data_var"])):
        widgets_stock[team_name]["registration_data"][i][0].grid_remove()
        widgets_stock[team_name]["registration_data"][i][1].grid_remove()
        widgets_stock[team_name]["registration_data"][i][2].grid_remove()
        widgets_stock[team_name]["registration_data"][i][3].grid_remove()
        widgets_stock[team_name]["registration_data"][i][4].grid_remove()
        widgets_stock[team_name]["registration_data"][i][5].grid_remove()
    if is_button_checked(0, team_name):
        uncheck_all_buttons(team_name)
        for i in range(1, len(widgets_stock[team_name]["registration_data_var"])):
            username = widgets_stock[team_name]["registration_data_var"][i][1].get()
            if not delete_member(username):
                widgets_stock[team_name]["buttons"]["label"].configure(text=messages_bundle["unexpected_error"])
                conn.rollback()
                break
        widgets_stock[team_name]["registration_data_var"] = []
        widgets_stock[team_name]["registration_data"] = []
        widgets_stock[team_name]["state_dict"]["email_state"] = []
        widgets_stock[team_name]["state_dict"]["phone_number_state"] = []
        widgets_stock[team_name]["state_dict"]["first_name_state"] = []
        widgets_stock[team_name]["state_dict"]["last_name_state"] = []
        widgets_stock[team_name]["buttons"]["delete"].configure(state="disabled")
        widgets_stock[team_name]["buttons"]["update"].configure(state="disabled")
        widgets_stock[team_name]["buttons"]["clear"].configure(state="disabled")
        widgets_stock[team_name]["buttons"]["label"].configure(text=messages_bundle["delete_done"])
    else:
        L = get_selected_buttons(team_name)
        L.sort(reverse = True)
        uncheck_all_buttons(team_name)
        for i in L:
            username = widgets_stock[team_name]["registration_data_var"][i][1].get()
            if not delete_member(username):
                widgets_stock[team_name]["buttons"]["label"].configure(text=messages_bundle["unexpected_error"])
                conn.rollback()
                break
            widgets_stock[team_name]["registration_data_var"].remove(widgets_stock[team_name]["registration_data_var"][i])
            widgets_stock[team_name]["registration_data"].remove(widgets_stock[team_name]["registration_data"][i])
            widgets_stock[team_name]["state_dict"]["email_state"].remove(widgets_stock[team_name]["state_dict"]["email_state"][i-1])
            widgets_stock[team_name]["state_dict"]["phone_number_state"].remove(widgets_stock[team_name]["state_dict"]["phone_number_state"][i-1])
            widgets_stock[team_name]["state_dict"]["first_name_state"].remove(widgets_stock[team_name]["state_dict"]["first_name_state"][i-1])
            widgets_stock[team_name]["state_dict"]["last_name_state"].remove(widgets_stock[team_name]["state_dict"]["last_name_state"][i-1])
        for i in range(1,len(widgets_stock[team_name]["registration_data_var"])):
            widgets_stock[team_name]["registration_data"][i][0].grid(row=i, column=0, padx=15, sticky='ew')
            widgets_stock[team_name]["registration_data"][i][0].configure(command = lambda index = i, name=team_name : check_delete(index, team_name))
            widgets_stock[team_name]["registration_data"][i][1].grid(row=i, column=1, padx=0, sticky='ew')
            widgets_stock[team_name]["registration_data"][i][2].grid(row=i, column=2, padx=10, sticky='ew')
            widgets_stock[team_name]["registration_data"][i][3].grid(row=i, column=3, padx=10, sticky='ew')
            widgets_stock[team_name]["registration_data"][i][4].grid(row=i, column=4, padx=10, sticky='ew')
            widgets_stock[team_name]["registration_data"][i][5].grid(row=i, column=5, padx=10, sticky='ew')
            name = widgets_stock[team_name]["registration_data"][i][1].winfo_name()
            widgets_stock[team_name]["lines"][name] = i
        if update_active(team_name):
            widgets_stock[team_name]["buttons"]["update"].configure(state="active")
        else:
            widgets_stock[team_name]["buttons"]["update"].configure(state="disabled")
        widgets_stock[team_name]["buttons"]["delete"].configure(state="disabled")
        widgets_stock[team_name]["buttons"]["label"].configure(text=messages_bundle["delete_done"])
    conn.commit()
    widgets_stock[team_name]["modif"].update()


def _buttons_frm_widgets(buttons_frm, team_name):
    update_btn = ttk.Button(buttons_frm, text=messages_bundle["edit_button"], command = lambda name = team_name: update_function(team_name))
    update_btn.grid(row=0, column=0, padx=10)
    update_btn.configure(state="disabled")
    widgets_stock[team_name]["buttons"]["update"] = update_btn
    clear_btn = ttk.Button(buttons_frm, text=messages_bundle["clear_button"], command = lambda name = team_name: clear_function(team_name))
    clear_btn.grid(row=0, column=1, padx=10)
    clear_btn.configure(state="disabled")
    widgets_stock[team_name]["buttons"]["clear"] = clear_btn
    delete_btn = ttk.Button(buttons_frm, text=messages_bundle["delete_button"], command = lambda name = team_name: delete_function(team_name))
    delete_btn.grid(row=0, column=2, padx=10)
    delete_btn.configure(state="disabled")
    widgets_stock[team_name]["buttons"]["delete"] = delete_btn
    label_btn = tk.Label(buttons_frm, text="", background="#dcdad5", foreground="red")
    label_btn.grid(row=1, columnspan=3)
    widgets_stock[team_name]["buttons"]["label"] = label_btn


def _add_members(scroll_registration_frm, team_name, data):
    widgets_stock[team_name]["registration_data"].append(
    (
        ttk.Checkbutton(scroll_registration_frm, width=5, variable=widgets_stock[team_name]["registration_data_var"][0][0], \
            command= lambda name = team_name : check_all_selected(name)),
        ttk.Label(scroll_registration_frm, anchor=tk.CENTER, \
            text=messages_bundle["username"], style="Header.TLabel"), 
        ttk.Label(scroll_registration_frm, anchor=tk.CENTER, \
            text=messages_bundle["first_name"], style="Header.TLabel"),
        ttk.Label(scroll_registration_frm, anchor=tk.CENTER, \
            text=messages_bundle["last_name"], style="Header.TLabel"),
        ttk.Label(scroll_registration_frm, anchor=tk.CENTER, \
            text=messages_bundle["email_address"], style="Header.TLabel"),
        ttk.Label(scroll_registration_frm, anchor=tk.CENTER, \
            text=messages_bundle["phone_number"], style="Header.TLabel")
    )
    )

    # We add the widgets to the frame.
    widgets_stock[team_name]["registration_data"][0][0].grid(row=0, column=0, padx=15, pady=5, sticky='ew')
    widgets_stock[team_name]["registration_data"][0][1].grid(row=0, column=1, padx=0, pady=5, sticky='ew')
    widgets_stock[team_name]["registration_data"][0][2].grid(row=0, column=2, pady=5, sticky='ew')
    widgets_stock[team_name]["registration_data"][0][3].grid(row=0, column=3, pady=5, sticky='ew')
    widgets_stock[team_name]["registration_data"][0][4].grid(row=0, column=4, pady=5, sticky='ew')
    widgets_stock[team_name]["registration_data"][0][5].grid(row=0, column=5, pady=5, sticky='ew')

    for i in range(len(data)):
        widgets_stock[team_name]["registration_data_var"].append(
            (
                tk.IntVar(),
                tk.StringVar(),
                tk.StringVar(),
                tk.StringVar(),
                tk.StringVar(),
                tk.StringVar()
            )
        )

        # The values loaded from the database.
        username = "" if data[i][0] is None else data[i][0]
        first_name = "" if data[i][1] is None else data[i][1]
        last_name = "" if data[i][2] is None else data[i][2]
        email = "" if data[i][3] is None else data[i][3]
        phone_number = "" if data[i][4] is None else data[i][4]

        widgets_stock[team_name]["registration_data_var"][i+1][1].set(username)
        widgets_stock[team_name]["registration_data_var"][i+1][2].set(first_name)
        widgets_stock[team_name]["registration_data_var"][i+1][3].set(last_name)
        widgets_stock[team_name]["registration_data_var"][i+1][4].set(email)
        widgets_stock[team_name]["registration_data_var"][i+1][5].set(phone_number)
        widgets_stock[team_name]["lines"]["line_{}".format(i+1)] = i+1

        widgets_stock[team_name]["registration_data_var"][i+1][2].trace('w', lambda *args, passed = "line_{}".format(i+1), name = team_name : modified_entry("first_name_state", passed, name, *args))
        widgets_stock[team_name]["registration_data_var"][i+1][3].trace('w', lambda *args, passed = "line_{}".format(i+1), name = team_name : modified_entry("last_name_state", passed, name, *args))
        widgets_stock[team_name]["registration_data_var"][i+1][4].trace('w', lambda *args, passed = "line_{}".format(i+1), name = team_name : trace_email(passed, name, *args))
        widgets_stock[team_name]["registration_data_var"][i+1][5].trace('w', lambda *args, passed = "line_{}".format(i+1), name = team_name : trace_phone(passed, name, *args))

        # The values loaded from the database are stored in the list 
        # current_student_registrations. This way, we can track the values that the user
        # changed.
        widgets_stock[team_name]["current_student_registrations"].append((username, first_name, last_name, email, phone_number))
        # We add the four widgets to the list registration_data.
        widgets_stock[team_name]["registration_data"].append(
            (
                ttk.Checkbutton(scroll_registration_frm, width=5, variable=widgets_stock[team_name]["registration_data_var"][i+1][0], command = lambda index = i+1, name=team_name : check_delete(index, team_name)),
                tk.Label(scroll_registration_frm, name="line_{}".format(i+1), justify='center', text=username, width=20),
                tk.Entry(scroll_registration_frm, justify='center', textvariable=widgets_stock[team_name]["registration_data_var"][i+1][2], highlightbackground="#dcdad5"),
                tk.Entry(scroll_registration_frm, justify='center', textvariable=widgets_stock[team_name]["registration_data_var"][i+1][3], highlightbackground="#dcdad5"),
                tk.Entry(scroll_registration_frm, justify='center', textvariable=widgets_stock[team_name]["registration_data_var"][i+1][4], highlightbackground="#dcdad5"),
                tk.Entry(scroll_registration_frm, justify='center', textvariable=widgets_stock[team_name]["registration_data_var"][i+1][5], highlightbackground="#dcdad5")
            )
        )
        # The four widgets are added to the frame.
        widgets_stock[team_name]["registration_data"][i+1][0].grid(row=i+1, column=0, padx=15, sticky='ew')
        widgets_stock[team_name]["registration_data"][i+1][1].grid(row=i+1, column=1, padx=0, sticky='ew')
        widgets_stock[team_name]["registration_data"][i+1][2].grid(row=i+1, column=2, padx=10, sticky='ew')
        widgets_stock[team_name]["registration_data"][i+1][3].grid(row=i+1, column=3, padx=10, sticky='ew')
        widgets_stock[team_name]["registration_data"][i+1][4].grid(row=i+1, column=4, padx=10, sticky='ew')
        widgets_stock[team_name]["registration_data"][i+1][5].grid(row=i+1, column=5, padx=10, sticky='ew')

        widgets_stock[team_name]["state_dict"]["first_name_state"].append(INIT_STATE)
        widgets_stock[team_name]["state_dict"]["last_name_state"].append(INIT_STATE)
        widgets_stock[team_name]["state_dict"]["email_state"].append(INIT_STATE)
        widgets_stock[team_name]["state_dict"]["phone_number_state"].append(INIT_STATE)


def delete_member(username):
    try:
        cursor.execute("DELETE FROM Members WHERE username=?", (username, ))
        cursor.execute("DELETE FROM Login WHERE username=?", (username, ))
    except sqlite3.Error as error:
        print(error)
        return False
    return True


def update_member(team_name, i):
    info = {}
    K = list(widgets_stock[team_name]["state_dict"].keys())
    for j in range(len(K)):
        if widgets_stock[team_name]["state_dict"][K[j]][i-1] == GOOD_MODIF:
            l = len(K[j])
            info[K[j]] = widgets_stock[team_name]["registration_data_var"][i][j+2].get()
    if len(info)>0:
        query = generate_query(info)
        var = list(info.values())
        var.append(widgets_stock[team_name]["registration_data_var"][i][1].get())
        try:
            cursor.execute(query, tuple(var))
        except sqlite3.Error as error:
            print(error)
            return False
        return True
    else:
        return True


def generate_query(dico):
    query = "UPDATE Members SET "
    L = [convertion[elem] for elem in list(dico.keys())]
    L.sort()
    for i in range(len(L)-1):
        query+=convertion[L[i]][:-6]
        query+="=?, "
    query+=convertion[L[-1]][:-6]
    query+="=? WHERE username=?"
    return query


def update_function(team_name):
    cursor.execute("BEGIN")
    for i in range(1, len(widgets_stock[team_name]["registration_data_var"])):
        if not update_member(team_name, i):
            widgets_stock[team_name]["buttons"]["label"].configure(text=messages_bundle["unexpected_error"])
            conn.rollback()
            break
    conn.commit()
    widgets_stock[team_name]["buttons"]["label"].configure(text=messages_bundle["update_done"])
    set_init(team_name)
    widgets_stock[team_name]["buttons"]["update"].configure(state="disabled")
    widgets_stock[team_name]["buttons"]["clear"].configure(state="disabled")


def set_init(team_name):
    for i in range(len(widgets_stock[team_name]["state_dict"]["email_state"])):
        for elem in list(widgets_stock[team_name]["state_dict"].keys()):
            widgets_stock[team_name]["state_dict"][elem][i] = INIT_STATE


def clear_function(team_name):
    for i in range(1, len(widgets_stock[team_name]["registration_data_var"])):
        username = widgets_stock[team_name]["registration_data_var"][i][1].get()
        L = [convertion[elem] for elem in list(widgets_stock[team_name]["state_dict"].keys())]
        L.sort()
        for j in L:
            key = convertion[j]
            if widgets_stock[team_name]["state_dict"][key][i-1]!=INIT_STATE:
                cursor.execute("SELECT {} FROM Members WHERE username=?".format(key[:-6]), (username,))
                item = cursor.fetchone()
                widgets_stock[team_name]["registration_data"][i][j].grid_remove()
                widgets_stock[team_name]["registration_data_var"][i][j].set(item)
                widgets_stock[team_name]["registration_data"][i][j].configure(highlightbackground="#dcdad5")
                widgets_stock[team_name]["registration_data"][i][j].grid(row=i, column=j, padx=10, sticky='ew')
                widgets_stock[team_name]["state_dict"][key][i-1] = INIT_STATE
    widgets_stock[team_name]["buttons"]["clear"].configure(state="disabled")
    widgets_stock[team_name]["buttons"]["update"].configure(state="disabled")
    widgets_stock[team_name]["buttons"]["label"].configure(text="")
    widgets_stock[team_name]["modif"].update()


def warning_window():
    window = tk.Tk()
    frame = tk.Frame(window)
    label = tk.Label(frame, text=messages_bundle["sure_exit"])
    label.grid(row=0, columnspan=3)
    btn_save = tk.Button(frame, text=messages_bundle["save"])
    btn_save.grid(row=1, column=0)
    btn_cancel = tk.Button(frame, text=messages_bundle["cancel_button"])
    btn_cancel.grid(row=1, column=1)
    btn_quit = tk.Button(frame, text=messages_bundle["exit"])
    btn_quit.grid(row=1, column=2)
    frame.pack()
    window.mainloop()
