import tkinter as tk
from tkinter import ttk
import sqlite3
import sys, os
import csv
sys.path.append('.')
import gui.gui_config as config
from utils import load_check_image, load_cadenas_image, password_ok, random_pwd
from authentication import encrypt_password

messages_bundle = {}
labels = {}
control_labels = {}
buttons = {"already_exists" : False}
check_image = None
cadenas_image = None
team_list = []

INIT_STATE = 0
WRONG_MODIF = 1
GOOD_MODIF = 2

def add_new_member(_messages_bundle, curs, connex, root=None, team_name=None, to_update=None):

    global messages_bundle
    global cursor
    global conn
    global members
    global check_image
    global cadenas_image
    global team_list

    messages_bundle = _messages_bundle
    cursor = curs
    conn = connex
    members = {}
    fill_members()

    check_image = load_check_image()
    cadenas_image = load_cadenas_image()
    team_list = get_team_names()
    if root is None:
        window = tk.Toplevel()
        window.geometry('600x450')
        config.configure_style()
        member_frm = ttk.Frame(window, style="Tab.TFrame")
        member_frm_widgets(member_frm)
        member_frm.pack()

        team_frm = ttk.Frame(window, style="Tab.TFrame")
        team_frm_widgets(team_frm)
        team_frm.pack()

        button_frm = ttk.Frame(window, style="Tab.TFrame")
        button_frm_widgets(window, button_frm, True, to_update)
        button_frm.pack()

        window.mainloop()
    else:
        window=ttk.Frame(root, style="Tab.TFrame",height=400, width=900)
        
        member_frm = ttk.Frame(window, style="Tab.TFrame")
        member_frm_widgets(member_frm)
        member_frm.pack()

        team_frm = ttk.Frame(window, style="Tab.TFrame")
        team_frm_widgets(team_frm)
        team_frm.pack()

        button_frm = ttk.Frame(window, style="Tab.TFrame")
        button_frm_widgets(window, button_frm, False, to_update)
        button_frm.pack()

        window.pack()


def member_frm_widgets(root):
    member_frm = root
    ttk.Label(member_frm, text=messages_bundle["username"], style="Header2.TLabel", anchor="center").grid(row=0, padx=10, pady=10)
    ttk.Label(member_frm, text=messages_bundle["password"], style="Header2.TLabel", anchor="center").grid(row=1, padx=10, pady=10)
    ttk.Label(member_frm, text=messages_bundle["first_name"], style="Header2.TLabel", anchor="center").grid(row=2, padx=10, pady=10)
    ttk.Label(member_frm, text=messages_bundle["last_name"], style="Header2.TLabel", anchor="center").grid(row=3, padx=10, pady=10)
    ttk.Label(member_frm, text=messages_bundle["email_address"], style="Header2.TLabel", anchor="center").grid(row=4, padx=10, pady=10)
    ttk.Label(member_frm, text=messages_bundle["phone_number"], style="Header2.TLabel", anchor="center").grid(row=5, padx=10, pady=10)
    ttk.Label(member_frm, text=messages_bundle["experience"], style="Header2.TLabel", anchor="center").grid(row=6, padx=10, pady=10)

    user_name = tk.StringVar("")

    listeOption = list(members.keys())
    enter_user_name = ttk.Combobox(member_frm, values=listeOption, textvariable=user_name)
    enter_user_name.grid(row=0, column=1, padx=10, pady=10)

    member_frm.pack(fill="both", expand=True)

    user_name.trace('w', aux_enter_user_name)
    labels["user_name"] = [user_name, enter_user_name, INIT_STATE]

    password_entry_vay = tk.StringVar("")
    password_entry = tk.Entry(member_frm, textvariable=password_entry_vay, state="disabled", highlightbackground="#f1f1f1", show='*')
    password_entry.grid(row=1, column=1)
    password_entry_vay.trace('w', trace_password_check)
    labels["password"] = [password_entry, password_entry_vay, INIT_STATE]

    first_entry_var = tk.StringVar("")
    first_entry = tk.Entry(member_frm, textvariable=first_entry_var, state="disabled", highlightbackground="#f1f1f1")
    first_entry.grid(row=2, column=1)
    first_entry_var.trace('w', lambda *args : modified_entry("first_name", 0, *args))
    labels["first_name"] = [first_entry, first_entry_var, INIT_STATE]

    last_entry_var = tk.StringVar("")
    last_entry = tk.Entry(member_frm, textvariable=last_entry_var, state="disabled", highlightbackground="#f1f1f1")
    last_entry.grid(row=3, column=1)
    last_entry_var.trace('w', lambda *args : modified_entry("last_name", 1, *args))
    labels["last_name"] = [last_entry, last_entry_var, INIT_STATE]

    email_entry_var = tk.StringVar("")
    email_entry = tk.Entry(member_frm, textvariable=email_entry_var, state="disabled", highlightbackground="#f1f1f1")
    email_entry.grid(row=4, column=1)
    email_entry_var.trace('w', trace_email)
    labels["email"] = [email_entry, email_entry_var, INIT_STATE]

    phone_entry_var = tk.StringVar("")
    phone_entry = tk.Entry(member_frm, textvariable=phone_entry_var, state="disabled", highlightbackground="#f1f1f1")
    phone_entry.grid(row=5, column=1)
    phone_entry_var.trace('w', trace_phone)
    labels["phone_number"] = [phone_entry, phone_entry_var, INIT_STATE]

    exp_var = tk.DoubleVar()
    exp_var.set(0)
    exp = tk.Scale(member_frm, orient='horizontal', from_=0, to=10, resolution=1, length=230, state="disabled", background="#f1f1f1", variable=exp_var)
    exp.grid(row=6, column=1)
    exp_var.trace('w', trace_experience)
    labels["experience"] = [exp, exp_var, INIT_STATE]

    user_label_ctrl = ttk.Label(member_frm, text=messages_bundle["choose_user"], style="Check.TLabel", width=30, anchor="center")
    user_label_ctrl.grid(row=0, column=2)
    control_labels["user_name"] = user_label_ctrl

    password_button = ttk.Button(member_frm, text=messages_bundle["change_mdp"], state="disabled", image=cadenas_image, command=new_password)
    password_button.image=cadenas_image
    password_button.grid(row=1, column=2)
    control_labels["password"] = password_button

    first_label_ctrl = ttk.Label(member_frm, text="", style="Check.TLabel", width=30, anchor="center")
    first_label_ctrl.grid(row=2, column=2)
    control_labels["first_name"] = first_label_ctrl

    last_label_ctrl = ttk.Label(member_frm, text="", style="Check.TLabel", width=30, anchor="center")
    last_label_ctrl.grid(row=3, column=2)
    control_labels["last_name"] = last_label_ctrl

    email_label_ctrl = ttk.Label(member_frm, text="", style="Check.TLabel", width=30, anchor="center")
    email_label_ctrl.grid(row=4, column=2)
    control_labels["email"] = email_label_ctrl

    phone_label_ctrl = ttk.Label(member_frm, text="", style="Check.TLabel", width=30, anchor="center")
    phone_label_ctrl.grid(row=5, column=2)
    control_labels["phone_number"] = phone_label_ctrl

    member_frm.columnconfigure(0, weight=1)
    member_frm.columnconfigure(1, weight=2)
    member_frm.columnconfigure(2, weight=1)


def aux_enter_user_name(*args):
    entry = labels["user_name"][0].get()
    if est_valide(entry):
        control_labels["user_name"].configure(text="", image = check_image)
        control_labels["user_name"].image=check_image
        buttons["clear"].configure(state="normal")
        if entry in list(members.keys()):
            buttons["already_exists"] = True
            existing_member_selected(entry)
            control_labels["error"].configure(text="")
        else:
            buttons["already_exists"] = False
            new_member()
            control_labels["error"].configure(text="")
    else:
        control_labels["user_name"].configure(text=messages_bundle["choose_user"], image = "")
        buttons["clear"].configure(state="disabled")
        buttons["already_exists"] = False
        control_labels["error"].configure(text="")
    if len(entry)>0:
        buttons["clear"].configure(state="normal")


def new_member():
    labels["first_name"][0].configure(state="normal")
    control_labels["password"].configure(state="normal")
    labels["password"][2] = WRONG_MODIF
    labels["last_name"][0].configure(state="normal")
    control_labels["last_name"].configure(text=messages_bundle["correct_error"])
    control_labels["first_name"].configure(text=messages_bundle["correct_error"])
    control_labels["email"].configure(text=messages_bundle["enter_email_address"])
    control_labels["phone_number"].configure(text=messages_bundle["enter_phone_number"])
    labels["email"][0].configure(state="normal")
    labels["phone_number"][0].configure(state="normal")
    labels["experience"][0].configure(state="normal")
    buttons[team_list[0][0]][2] = WRONG_MODIF
    buttons["apply"].configure(state="disabled")
    buttons["reset"].configure(state="disabled")


def team_frm_widgets(root):
    team_frm = root
    varGr = tk.StringVar("")
    varGr.trace('w', trace_team)
    for i in range(len(team_list)):
        ck_btn = ttk.Radiobutton(team_frm, variable=varGr, text=team_list[i][0], value=team_list[i][0], style="TRadiobutton")
        ck_btn.grid(row=0, column=i, padx=10, pady=5)
        team_frm.columnconfigure(i, weight=1)
        buttons[team_list[i][0]] = [ck_btn, varGr, INIT_STATE]

    team_frm.pack(fill="both", expand=True)


def button_frm_widgets(window, root, vrai, to_update=None):
    btn_frame = root
    if vrai:
        label_error = ttk.Label(btn_frame, text="", style="Check.TLabel")
        label_error.grid(row=0, columnspan=4, padx=10, pady=5)
        control_labels["error"] = label_error
        cancel_btn = ttk.Button(btn_frame, text=messages_bundle["cancel_button"], command = lambda fenetre=window : commande_cancel(fenetre))
        cancel_btn.grid(row=1, column=0)
        buttons["cancel"] = cancel_btn
        clear_btn = ttk.Button(btn_frame, text=messages_bundle["clear_button"], state="disabled", command=commande_clear)
        clear_btn.grid(row=1, column=1)
        buttons["clear"] = clear_btn
        reset_btn = ttk.Button(btn_frame, text=messages_bundle["reset_button"], state="disabled", command=commande_reset)
        reset_btn.grid(row=1, column=2)
        buttons["reset"] = reset_btn
        apply_btn = ttk.Button(btn_frame, text=messages_bundle["apply_button"], state="disabled", command=lambda: commande_apply(window, to_update))
        apply_btn.grid(row=1, column=3)
        buttons["apply"] = apply_btn
        for i in range(4):
            btn_frame.columnconfigure(i, weight=1)
    else:
        label_error = ttk.Label(btn_frame, text="", style="Check.TLabel")
        label_error.grid(row=0, columnspan=3, padx=10, pady=5)
        control_labels["error"] = label_error
        clear_btn = ttk.Button(btn_frame, text=messages_bundle["clear_button"], state="disabled", command=commande_clear)
        clear_btn.grid(row=1, column=0)
        buttons["clear"] = clear_btn
        reset_btn = ttk.Button(btn_frame, text=messages_bundle["reset_button"], state="disabled", command=commande_reset)
        reset_btn.grid(row=1, column=1)
        buttons["reset"] = reset_btn
        apply_btn = ttk.Button(btn_frame, text=messages_bundle["apply_button"], state="disabled", command= lambda:commande_apply(None))
        apply_btn.grid(row=1, column=2)
        buttons["apply"] = apply_btn
        for i in range(3):
            btn_frame.columnconfigure(i, weight=1)
    btn_frame.pack(fill="both", expand=True)


def fill_members():
    cursor.execute("SELECT * FROM Members")
    res = cursor.fetchall()
    for elem in res:
        members[elem[0]] = list(elem)[1:]


def get_team_names():
    cursor.execute("SELECT DISTINCT team_name FROM Teams")
    res = cursor.fetchall()
    return list(res)


def est_valide(username):
    if len(username)>0:
        if username[0].isalpha():
            for i in range(1,len(username)-1):
                if username[i]=="_":
                    if username[i+1].isalpha():
                        return True
    return False


def existing_member_selected(username):
    labels["first_name"][1].set(members[username][0])
    labels["first_name"][0].configure(state="normal")
    labels["password"][1].set("***********")
    control_labels["password"].configure(state="normal")
    labels["last_name"][1].set(members[username][1])
    labels["last_name"][0].configure(state="normal")
    labels["email"][1].set(members[username][2])
    labels["email"][0].configure(state="normal")
    labels["phone_number"][1].set(members[username][3])
    labels["phone_number"][0].configure(state="normal")
    labels["experience"][1].set(int(members[username][4]))
    labels["experience"][0].configure(state="normal")
    buttons[members[username][5]][1].set(members[username][5])
    buttons["apply"].configure(state="disabled")
    buttons["reset"].configure(state="disabled")


def lemailestbon(mail):
    liste = list(mail)
    if '.' not in liste :
        return False
    card_at = 0
    for e in liste :
        if e =='@':
            card_at += 1
    if card_at != 1 :
        return False

    indice_at = 0
    dernier_indice_pt = 0
    if liste[-1] == '.' :
        return False

    for i in range(len(liste)-1):
        if liste[i] == '@':
            indice_at = i
        if liste[i] == '.':
            if dernier_indice_pt == i-1 or liste[i+1] == '@':
                return False
            dernier_indice_pt = i

    if dernier_indice_pt < indice_at:
        return False

    return True


def trace_phone(*args):
    phone_number = labels["phone_number"][1].get().strip()
    buttons["apply"].configure(state="disabled")
    buttons["reset"].configure(state="disabled")
    control_labels["error"].configure(text="")
    l = list(phone_number)
    vrai = True
    if len(l)!=10 : 
        vrai = False
    for elem in l:
        if elem not in [str(k) for k in range(10)]:
            vrai = False
    if vrai :
        if buttons["already_exists"]:
            if phone_number == members[labels["user_name"][0].get()][3]:
                labels["phone_number"][2] = INIT_STATE
                control_labels["phone_number"].configure(text="", image ="")
            else:
                labels["phone_number"][2] = GOOD_MODIF
                control_labels["phone_number"].configure(text="", image = check_image)
                control_labels["phone_number"].image=check_image
            if update_active():
                buttons["apply"].configure(state="normal")
                control_labels["error"].configure(text="Apply?")
        else:
            labels["phone_number"][2] = GOOD_MODIF
            control_labels["phone_number"].configure(text="", image = check_image)
            control_labels["phone_number"].image=check_image
        labels["phone_number"][0].configure(highlightbackground="#f1f1f1")

    else:
        labels["phone_number"][2] = WRONG_MODIF
        labels["phone_number"][0].configure(highlightbackground="red")
        control_labels["phone_number"].configure(text=messages_bundle["enter_phone_number"], image="")
    buttons["clear"].configure(state="normal")
    if buttons["already_exists"]:
        if not is_init():
            buttons["reset"].configure(state="normal")


def trace_email(*args):
    email = labels["email"][1].get().strip()
    buttons["apply"].configure(state="disabled")
    buttons["reset"].configure(state="disabled")
    control_labels["error"].configure(text="")
    if lemailestbon(email) :
        if buttons["already_exists"]:
            if email == members[labels["user_name"][0].get()][2]:
                labels["email"][2] = INIT_STATE
                control_labels["email"].configure(text="", image ="")
            else:
                labels["email"][2] = GOOD_MODIF
                control_labels["email"].configure(text="", image = check_image)
                control_labels["email"].image=check_image
            if update_active():
                buttons["apply"].configure(state="normal")
                control_labels["error"].configure(text=messages_bundle["apply"])
        else:
            labels["email"][2] = GOOD_MODIF
            control_labels["email"].configure(text="", image = check_image)
            control_labels["email"].image=check_image
        labels["email"][0].configure(highlightbackground="#f1f1f1")
    else:
        labels["email"][2] = WRONG_MODIF
        labels["email"][0].configure(highlightbackground="red")
        control_labels["email"].configure(text=messages_bundle["enter_email_address"], image="")
    buttons["clear"].configure(state="normal")
    if buttons["already_exists"]:
        if not is_init():
            buttons["reset"].configure(state="normal")


def trace_password_check(*args):
    if len(labels["password"][1].get())>0:
        if update_active():
            buttons["apply"].configure(state="normal")
            control_labels["error"].configure(text=messages_bundle["apply"])


def update_active():
    if is_init():
        # ajouter peut-être des commandes pour vider les control_labels
        control_labels["error"].configure(text="")
        return False
    else:
        if is_wrong():
            # idem
            control_labels["error"].configure(text="")
            return False
        else:
            # idem
            return True


def is_init():
    for key in list(labels.keys()):
        if labels[key][2]!=INIT_STATE:
            return False
    for nom in team_list:
        if buttons[nom[0]][2]!=INIT_STATE:
            return False
    return True


def is_wrong():
    for key in list(labels.keys()):
        if labels[key][2]==WRONG_MODIF:
            return True
    for nom in team_list:
        if buttons[nom[0]][2]==WRONG_MODIF:
            return True
    return False


def modified_entry(type_entry, num, *args):
    entry = labels[type_entry][1].get().strip()
    buttons["apply"].configure(state="disabled")
    buttons["reset"].configure(state="disabled")
    control_labels["error"].configure(text="")
    if len(entry)>0:
        if buttons["already_exists"]:
            if entry == members[labels["user_name"][0].get()][num]:
                labels[type_entry][2] = INIT_STATE
                control_labels[type_entry].configure(text="", image ="")
            else:
                labels[type_entry][2] = GOOD_MODIF
                control_labels[type_entry].configure(text="", image = check_image)
                control_labels[type_entry].image=check_image
            if update_active():
                buttons["apply"].configure(state="normal")
                control_labels["error"].configure(text=messages_bundle["apply"])
        else:
            labels[type_entry][2] = GOOD_MODIF
            control_labels[type_entry].configure(text="", image = check_image)
            control_labels[type_entry].image=check_image
        labels[type_entry][0].configure(highlightbackground="#f1f1f1")
    else:
        labels[type_entry][2] = WRONG_MODIF
        control_labels[type_entry].configure(text=messages_bundle["correct_error"], image="")
        labels[type_entry][0].configure(highlightbackground="red")
    buttons["clear"].configure(state="normal")
    if buttons["already_exists"]:
        if not is_init():
            buttons["reset"].configure(state="normal")


def commande_reset():
    username = labels["user_name"][0].get()
    existing_member_selected(username)
    buttons["apply"].configure(state="disabled")
    buttons["reset"].configure(state="disabled")
    control_labels["error"].configure(text="")


def trace_experience(*args):
    exp = labels["experience"][1].get()
    buttons["apply"].configure(state="disabled")
    buttons["reset"].configure(state="disabled")
    control_labels["error"].configure(text="")
    if buttons["already_exists"]:
        if exp!=members[labels["user_name"][0].get()][4]:
            labels["experience"][2] = GOOD_MODIF
        if not is_init():
            buttons["reset"].configure(state="normal")
    if update_active():
        buttons["apply"].configure(state="normal")
        control_labels["error"].configure(text=messages_bundle["apply"])


def trace_team(*args):
    team = buttons[team_list[0][0]][1].get()
    buttons["apply"].configure(state="disabled")
    buttons["reset"].configure(state="disabled")
    control_labels["error"].configure(text="")
    if buttons["already_exists"]:
        if team!=members[labels["user_name"][0].get()][5]:
            for nom in team_list:
                buttons[nom[0]][2]=GOOD_MODIF
        if not is_init():
            buttons["reset"].configure(state="normal")
    else:
        buttons[team_list[0][0]][2] = GOOD_MODIF
    if update_active():
        buttons["apply"].configure(state="normal")
        control_labels["error"].configure(text=messages_bundle["apply"])


def commande_cancel(root):
    root.destroy()


def commande_apply(root=None, window=None):
    if buttons["already_exists"]:
        username = labels["user_name"][0].get()
        if labels["password"][2]==GOOD_MODIF:
            password = labels["password"][1].get()
            try:
                cursor.execute("UPDATE Login SET password=? WHERE username=?", (encrypt_password(password), username))
                vrai1 = True
            except sqlite3.Error as error:
                print(error)
                control_labels["error"].configure(text=messages_bundle["unexpected_error"])
                vrai1 = False
        else:
            vrai1 = True
        query,L,vrai = generate_query()
        if len(L)>0:
            if vrai:
                var = [labels[elem][1].get() for elem in L[:-1]]
                var.append(buttons[team_list[0][0]][1].get())
            else:
                var = [labels[elem][1].get() for elem in L]
            var.append(username)
            try:
                cursor.execute(query, tuple(var))
                vrai2 = True
            except sqlite3.Error as error:
                print(error)
                control_labels["error"].configure(text=messages_bundle["unexpected_error"])
                vrai2 = False
        else:
            vrai2=True
    else:
        username = labels["user_name"][0].get()
        password = labels["password"][1].get()
        try:
            cursor.execute("INSERT INTO Login VALUES (?, ?, ?)", (username, encrypt_password(password), 0))
            vrai1 = True
        except sqlite3.IntegrityError as error:
            print(error)
            vrai1 = False
        firstname = labels["first_name"][1].get()
        lastname = labels["last_name"][1].get()
        email = labels["email"][1].get()
        phonenumber = labels["phone_number"][1].get()
        exp = labels["experience"][1].get()
        teamname = buttons[team_list[0][0]][1].get()
        try:
            cursor.execute("INSERT INTO Members VALUES (?, ?, ?, ?, ?, ?, ?)", (username, firstname, lastname, email, phonenumber, int(exp), teamname))
            vrai2 = True
        except sqlite3.IntegrityError as error:
            print(error)
            vrai2 = False
    if vrai1 and vrai2:
        conn.commit()
        control_labels["error"].configure(text=messages_bundle["applied"])
        buttons["apply"].configure(state="disabled")
        buttons["reset"].configure(state="disabled")
    else:
        conn.rollback()
    if window is not None:
        window.update()
    if root is not None:
        commande_cancel(root)


def generate_query():
    query = "UPDATE Members SET "
    L = []
    for elem in list(labels.keys()):
        if elem!="password" and labels[elem][2]==GOOD_MODIF:
            L.append(elem)
    if buttons[team_list[0][0]][2]==GOOD_MODIF:
        L.append("team_name")
        vrai = True
    else:
        vrai = False
    if len(L)>0:
        for i in range(len(L)-1):
            query+=L[i]
            query+="=?, "
        query+=L[-1]
        query+="=? WHERE username=?"
        return query, L, vrai
    else:
        return "",[], True


def commande_clear():
    for elem in list(labels.keys()):
        if elem=="user_name":
            labels[elem][0].set("")
            labels[elem][2] = INIT_STATE
        elif elem=="experience":
            labels[elem][1].set(0)
            labels[elem][0].configure(state="disabled")
            labels[elem][2] = INIT_STATE
        elif elem=="password":
            labels[elem][1].set("")
            labels[elem][2] = INIT_STATE
            control_labels["password"].configure(state="disabled")
        else:
            labels[elem][1].set("")
            labels[elem][0].configure(state="disabled", highlightbackground="#f1f1f1")
            labels[elem][2] = INIT_STATE
            control_labels[elem].configure(text="")
    buttons[team_list[0][0]][1].set("")
    for elem in team_list:
        buttons[elem[0]][2] = INIT_STATE
    buttons["clear"].configure(state="disabled")
    buttons["reset"].configure(state="disabled")
    buttons["apply"].configure(state="disabled")
    control_labels["error"].configure(text="")


def new_password():
    window = tk.Toplevel(bg="#f1f1f1")
    window.geometry('400x150')
    config.configure_style()

    frame = ttk.Frame(window, style="Tab.TFrame")
    frame.pack()

    label = ttk.Label(frame, text=messages_bundle["new_password"])
    label.grid(row=0, column=0, padx=10, pady=10)
    password_var = tk.StringVar("")
    password = ttk.Entry(frame, textvariable=password_var, show='*')
    password.grid(row=0, column=1, padx=10, pady=10)

    reveal_var = tk.IntVar()
    reveal_var.trace('w', lambda *args:trace_reveal(reveal_var, password, *args))
    reveal = ttk.Checkbutton(frame, text=messages_bundle["show"], variable=reveal_var, onvalue=1, style="Password.TCheckbutton")
    reveal.grid(row=1, columnspan=2, padx=10, pady=5)  

    control_label = ttk.Label(frame, text="", style='Check.TLabel')
    control_label.grid(row=2, columnspan=2, padx=10, pady=5)

    frame_button = ttk.Frame(window, style="Tab.TFrame")
    frame_button.pack()

    button_cancel = ttk.Button(frame_button, text=messages_bundle["cancel_button"], command=lambda:commande_cancel(window))
    button_cancel.grid(row=0, column=0, padx=10, pady=10)

    generate_button = ttk.Button(frame_button, text=messages_bundle["generate"], command=lambda:commande_generate(password_var, reveal_var))
    generate_button.grid(row=0, column=1, padx=10, pady=10)

    button_apply = ttk.Button(frame_button, text=messages_bundle["apply_button"], state="disabled", command=lambda:commande_apply_password(password_var, window))
    button_apply.grid(row=0, column=3, padx=10, pady=10)

    password_var.trace('w', lambda *args:trace_password(password_var, password, control_label, button_apply, *args))

    window.mainloop()


def trace_reveal(reveal_var, password, *args):
    value = int(reveal_var.get())
    if value==0:
        password.configure(show='*')
    else:
        password.configure(show='')


def trace_password(password_var, password, control_label, button_apply, *args):
    entry = password_var.get()
    if not password_ok(entry):
        control_label.configure(text=messages_bundle["enter_password"])
        button_apply.configure(state="disabled")
    else:
        control_label.configure(text=messages_bundle["apply"])
        button_apply.configure(state="normal")


def commande_generate(password_var, reveal_var):
    new_password = random_pwd()
    password_var.set(new_password)
    reveal_var.set(1)


def commande_apply_password(password_var, window):
    labels["password"][1].set(password_var.get())
    labels["password"][2] = GOOD_MODIF
    commande_cancel(window)
    if buttons["already_exists"]:
        buttons["reset"].configure(state="normal")
        if update_active():
                    buttons["apply"].configure(state="normal")
                    control_labels["error"].configure(text=messages_bundle["apply"])
