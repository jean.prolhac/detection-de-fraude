import tkinter as tk
from tkinter import ttk
import utils
import sqlite3
import sys
sys.path.append('.')
import gui.gui_config as config
from members.frame import add_widgets as reg_frame
from members.new_member import add_new_member as add_new

# The messages bundle.
messages_bundle = {}

# The language of the interface.
lang = None

# The object used to query the database.
cursor = None

# The object used to connect to the database.
conn = None

# The root window.
window = None

# The entries in the rh window.
entries = {}

# The control labels in the rh window.
control_label = {}

# The buttons in the rh window.
buttons = {}

# The windows.
windows = {}
teams = {}


def add_widgets(window, rh_tab, _messages_bundle, _cursor, _conn, _lang):

    global messages_bundle
    global cursor
    global windows
    global conn
    global lang
    
    # We assign the passed arguments to the global variables.
    messages_bundle = _messages_bundle
    cursor = _cursor
    conn = _conn
    lang = _lang
    windows = {"rh": rh_tab, 'window': window}
    config.configure_style()

    tablayout = ttk.Notebook(rh_tab)
    windows["notebook"] = tablayout

    current_config = utils.load_config()
    messages_bundle = utils.load_messages_bundle(current_config["bundle"] + current_config["lang"])
    add_current_teams(windows["notebook"])
    tab_add = ttk.Frame(windows["notebook"], height=400, width=900)
    tab_add.pack(fill="both")
    windows["notebook"].add(tab_add, text="+")

    def add_tab(event):
        all_tabs = windows["notebook"].tabs() # gets tuple of tab ids
        sel = windows["notebook"].select() # gets id of selected tab
        if sel == all_tabs[-1]:
            add_team_button(sel)

    tablayout.bind('<ButtonRelease-1>', add_tab)

    tablayout.pack()


def add_team_button(sel):
    create_window = tk.Toplevel()
    config.configure_style()
    create_window.title(messages_bundle["add_team"])
    create_window.resizable(False, False)
    windows["window_add"] = create_window

    root_frame=ttk.Frame(create_window, style="Tab.TFrame")

    entry_frame = ttk.Frame(root_frame, style="Tab.TFrame")
    _entry_frm_widgets(entry_frame)
    entry_frame.pack(fill="both", expand=True, padx=20, pady=10)

    message_frame = ttk.Frame(root_frame, style="Tab.TFrame")
    _message_frm_widgets(message_frame)
    message_frame.pack(fill="both", expand=True, padx=20, pady=10)

    buttons_frame = ttk.Frame(root_frame, style="Tab.TFrame")
    _buttons_frm_widgets(buttons_frame, sel)
    buttons_frame.pack(fill="both", expand=True, padx=20, pady=10)

    root_frame.pack()
    
    #init_state()
    create_window.mainloop()


def _entry_frm_widgets(entry_frame):
    ttk.Label(entry_frame, text = messages_bundle["label_add"], style="TLabel").grid(row=0, column = 0, padx=10, pady=10, sticky = "W")

    team_name_ent_var = tk.StringVar("")
    team_name_ent = ttk.Entry(entry_frame, textvariable=team_name_ent_var)
    team_name_ent.grid(row = 0, column = 1, sticky='ew')
    entries["team_name"] = (team_name_ent, team_name_ent_var)

    ttk.Label(entry_frame, text = messages_bundle["label_speciality"], style="TLabel").grid(row=1, column = 0, padx=10, pady=10, sticky = "W")
    team_speciality_ent_var = tk.StringVar("")
    listeOption = [messages_bundle["inspectors"], messages_bundle["others"]]
    team_speciality_ent = ttk.Combobox(entry_frame, values=listeOption, textvariable=team_speciality_ent_var)
    team_speciality_ent.grid(row = 1, column = 1, sticky='ew')
    entries["team_speciality"] = (team_speciality_ent, team_speciality_ent_var)

    entry_frame.columnconfigure(1, weight=1)


def _message_frm_widgets(message_frame):
    label = ttk.Label(message_frame, style="Check.TLabel")
    label.config(foreground="red")
    label.grid(row=0, column=0, sticky='ew')
    control_label["add_team_name"] = label


def _buttons_frm_widgets(buttons_frame, sel):
    button_create = ttk.Button(buttons_frame, text=messages_bundle["add_button"], command=lambda:create_team_button(sel))
    button_create.grid(row=0,column=0, padx=10, sticky='ew')
    buttons["add_team_name"] = button_create
    
    button_clear = ttk.Button(buttons_frame, text=messages_bundle["clear_button"], command=clear)
    button_clear.grid(row=0,column=1, padx=10, sticky='ew')
    buttons["clear_team_name"] = button_clear
    
    button_close = ttk.Button(buttons_frame, text=messages_bundle["cancel_button"], command=close)
    button_close.grid(row=0,column=2, padx=10, sticky='ew')
    buttons["close_team_name"] = button_close


def clear():
    entries["team_name"][0].delete(0,tk.END)
    entries["team_speciality"][0].delete(0,tk.END)
    control_label["add_team_name"].configure(text="")


def close():
    clear()
    windows["window_add"].destroy()
    windows['window'].update()


def create_team_button(sel):
    control_label["add_team_name"].configure(text="")
    team_name = entries["team_name"][0].get()
    speciality = entries["team_speciality"][0].get()
    if team_name=="":
        control_label["add_team_name"].configure(text=messages_bundle["team_name_missing"])
    else:
        if not create_team_db(team_name, speciality, cursor, conn):
            control_label["add_team_name"].configure(text=messages_bundle["tn_exists"])
        else:
            create_team(team_name, windows["notebook"], sel)
    close()


def create_team(team_name, tablayout, sel):
    if sel is None:
        tab_new_team = ttk.Frame(tablayout)
        tab_new_team.pack(fill="both")
        tablayout.add(tab_new_team, text=team_name)
        cursor.execute("SELECT username, first_name, last_name, email, phone_number FROM Members WHERE team_name=?", (team_name,))
        res = cursor.fetchall()
        if len(res)>0:
            reg_frame(tab_new_team, messages_bundle, res, team_name, cursor, conn)
        add_new_member = ttk.Button(tab_new_team, text=messages_bundle["add_new_member"], command= lambda passed = team_name : add_new(messages_bundle, cursor, conn, None, passed, windows['window']))
        add_new_member.pack(pady=2)
        add_new_member.configure(state="active")
        delete_team = ttk.Button(tab_new_team, text=messages_bundle["delete_team"], command= lambda passed = team_name : delete_team_command(passed))
        delete_team.pack(pady=2)
        delete_team.configure(state="active")
        rename_team = ttk.Button(tab_new_team, text=messages_bundle["rename_team"], command= lambda passed = team_name : rename_team_command(passed))
        rename_team.pack(pady=2)
        rename_team.configure(state="active")
        teams[team_name] = [tab_new_team, add_new_member, delete_team, rename_team]
    else:
        tablayout.tab(sel, text=team_name)
        tab_id = windows["rh"].nametowidget(sel)
        add_new_member = ttk.Button(tab_id, text=messages_bundle["add_new_member"], command= lambda passed = team_name : add_new(messages_bundle, cursor, conn, None, passed, windows['window']))
        add_new_member.pack(pady=2)
        add_new_member.configure(state="active")
        delete_team = ttk.Button(tab_id, text=messages_bundle["delete_team"], command= lambda passed = team_name : delete_team_command(passed))
        delete_team.pack(pady=2)
        delete_team.configure(state="active")
        rename_team = ttk.Button(tab_id, text=messages_bundle["rename_team"], command= lambda passed = team_name : rename_team_command(passed))
        rename_team.pack(pady=2)
        rename_team.configure(state="active")
        teams[team_name] = [tab_id, add_new_member, delete_team, rename_team]
        tablayout.add(tk.Frame(tablayout, height=400, width=900), text = '+')


def create_team_db(team_name, speciality, cursor, conn):
    try:
        cursor.execute("INSERT INTO Teams VALUES (?, ?)", (team_name,speciality))
    except sqlite3.IntegrityError as error:
        print(error)
        conn.rollback()
        return False
    conn.commit()
    return True


def add_current_teams(tablayout):
    cursor.execute("SELECT team_name FROM Teams")
    res = cursor.fetchall()
    for elem in res:
        create_team(elem[0],tablayout, None)


def rename_team_command(team_name):
    window_team = tk.Toplevel(bg="#f1f1f1")
    config.configure_style()
    rename_frm = ttk.Frame(window_team, style="Tab.TFrame")

    def trace_name(*args):
        var = name_var.get()
        if len(var)>0:
            btn_clear.configure(state="normal")
            if var not in list(teams.keys()):
                btn_apply.configure(state="normal")
                label_control.configure(text="")
            else:
                btn_apply.configure(state="disabled")
                label_control.configure(text=messages_bundle["tn_exists"])
        else:
            btn_clear.configure(state="disabled")
            btn_apply.configure(state="disabled")

    ttk.Label(rename_frm, text=messages_bundle["new_name"], style="TLabel").grid(row=0, column=0, padx=10, pady=10)
    name_var = tk.StringVar("")
    name_var.trace('w', trace_name)
    name = ttk.Entry(rename_frm, textvariable=name_var)
    name.grid(row=0, column=1, padx=10, pady=10)
    label_control = ttk.Label(rename_frm, text="", style="Check.TLabel")
    label_control.grid(row=1, columnspan=3, padx=10, pady=10)
    rename_frm.pack()

    def close():
        window_team.destroy()
    def clear():
        name_var.set("")
    def apply():
        team_name_new = name_var.get()
        cursor.execute("SELECT speciality FROM Teams WHERE team_name=?", (team_name,))
        speciality = cursor.fetchone()[0]
        cursor.execute("SELECT username FROM Members WHERE team_name=?", (team_name,))
        res = cursor.fetchall()
        vrai = True
        if create_team_db(team_name_new, speciality, cursor, conn):
            for elem in res:
                try:
                    cursor.execute("UPDATE Members SET team_name=? WHERE username=?", (team_name_new,elem[0]))
                except sqlite3.IntegrityError as error:
                    print(error)
                    label_control.configure(text=messages_bundle["unexpected_error"])
                    vrai = False
            try:
                cursor.execute("DELETE FROM Teams WHERE team_name=?", (team_name,))
            except sqlite3.IntegrityError as error:
                print(error)
                label_control.configure(text=messages_bundle["unexpected_error"])
                vrai = False
            if vrai:
                conn.commit()
                close()
            else:
                conn.callback()
        else:
            label_control.configure(text=messages_bundle["unexpected_error"])


    button_frm = ttk.Frame(window_team, style="Tab.TFrame")
    btn_close = ttk.Button(button_frm, text=messages_bundle["cancel_button"], command=close)
    btn_close.grid(row=0, column=0, padx=10, pady=10)
    btn_clear = ttk.Button(button_frm, text=messages_bundle["clear_button"], command=clear, state="disabled")
    btn_clear.grid(row=0, column=1, padx=10, pady=10)
    btn_apply = ttk.Button(button_frm, text=messages_bundle["apply_button"], command=apply, state="disabled")
    btn_apply.grid(row=0, column=2, padx=10, pady=10)
    button_frm.pack()

    window_team.mainloop()


def delete_team_command(team_name):    
    def apply():
        cursor.execute("SELECT username FROM Members WHERE team_name=?", (team_name,))
        res = cursor.fetchall()
        vrai = True
        for elem in res:
            try:
                cursor.execute("DELETE FROM Members WHERE username=?", (elem[0],))
                cursor.execute("DELETE FROM Login WHERE username=?", (elem[0],))
            except sqlite3.IntegrityError as error:
                print(error)
                vrai = False
        try:
            cursor.execute("DELETE FROM Teams WHERE team_name=?", (team_name,))
        except sqlite3.IntegrityError as error:
            print(error)
            vrai = False
        if vrai:
            conn.commit()
        else:
            conn.callback()
    
    MsgBox = tk.messagebox.askquestion(messages_bundle['warning'], messages_bundle['delete_error'], icon = 'error')
    if MsgBox == 'yes':
        apply()

